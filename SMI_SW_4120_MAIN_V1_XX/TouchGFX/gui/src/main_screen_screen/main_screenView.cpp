#include <gui/main_screen_screen/main_screenView.hpp>
#include "../../Core/Src/commands.h"
#include "../../Core/Src/menu.h"

/* External variables */
// Screen update flags
extern bool update_battery_flag;
extern bool update_title_flag;
extern bool update_mf_line_flag[9];
extern bool update_left_opt_flag, update_center_opt_flag, update_right_opt_flag;
extern bool display_splash_msg_flag, hide_splash_msg_flag;

// Buffers
extern uint32_t batt_percentage;
extern char title_buffer[20];
extern char left_opt_buffer[20];
extern char center_opt_buffer[20];
extern char right_opt_buffer[20];
extern char mf_line_buffer[9][100];
extern char sm_buffer[100], sm_title_buffer[20];

extern uint32_t stim_state;
extern bool stim_update_scr_flag, time_rem_update_flag;
uint16_t mf_line_update_idx = 0;
uint32_t tick_event_counter = 0;
extern uint32_t time_remaining;
uint32_t time_rem_min, time_rem_sec;

extern bool g1_update_rmm_flag, g2_update_rmm_flag;
extern uint8_t g1_rmm_buffer[4], g2_rmm_buffer[4];

main_screenView::main_screenView():
    invalidate_flag(false),
	tick_counter(0)
{}

void main_screenView::setupScreen()
{
    main_screenViewBase::setupScreen();
}

void main_screenView::tearDownScreen()
{
    main_screenViewBase::tearDownScreen();
}

void main_screenView::handleTickEvent()
{
	if(display_splash_msg_flag){
		Unicode::strncpy(ta_splash_msg_titleBuffer, sm_title_buffer, TA_SPLASH_MSG_TITLE_SIZE);
		ta_splash_msg_title.resizeToCurrentTextWithAlignment();
		ta_splash_msg_title.invalidate();
		Unicode::strncpy(ta_splash_msgBuffer, sm_buffer, TA_SPLASH_MSG_SIZE);
		ta_splash_msg.resizeToCurrentTextWithAlignment();
		ta_splash_msg.invalidate();
		sm.setVisible(true);
		sm.invalidate();
		display_splash_msg_flag = false;
	}
	else if(hide_splash_msg_flag){
		sm.setVisible(false);
		sm.invalidate();
		hide_splash_msg_flag = false;
	}
	else if(g1_update_rmm_flag){
		// Ch1
		if(g1_rmm_buffer[0] == RMM_POOR){
			res_ch1Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 50, 50));
			res_ch1.setPainter(res_ch1Painter);
			lbl_ch1.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		else if(g1_rmm_buffer[0] == RMM_MODERATE){
			res_ch1Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 250, 50));
			res_ch1.setPainter(res_ch1Painter);
			lbl_ch1.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
		}
		else if(g1_rmm_buffer[0] == RMM_GOOD){
			res_ch1Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 175, 0));
			res_ch1.setPainter(res_ch1Painter);
			lbl_ch1.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		res_ch1.invalidate();
		lbl_ch1.invalidate();
		//Ch2
		if(g1_rmm_buffer[1] == RMM_POOR){
			res_ch2Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 50, 50));
			res_ch2.setPainter(res_ch2Painter);
			lbl_ch2.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		else if(g1_rmm_buffer[1] == RMM_MODERATE){
			res_ch2Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 250, 50));
			res_ch2.setPainter(res_ch2Painter);
			lbl_ch2.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
		}
		else if(g1_rmm_buffer[1] == RMM_GOOD){
			res_ch2Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 175, 0));
			res_ch2.setPainter(res_ch2Painter);
			lbl_ch2.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		res_ch2.invalidate();
		lbl_ch2.invalidate();
		// Ch3
		if(g1_rmm_buffer[2] == RMM_POOR){
			res_ch3Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 50, 50));
			res_ch3.setPainter(res_ch3Painter);
			lbl_ch3.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		else if(g1_rmm_buffer[2] == RMM_MODERATE){
			res_ch3Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 250, 50));
			res_ch3.setPainter(res_ch3Painter);
			lbl_ch3.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
		}
		else if(g1_rmm_buffer[2] == RMM_GOOD){
			res_ch3Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 175, 0));
			res_ch3.setPainter(res_ch3Painter);
			lbl_ch3.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		res_ch3.invalidate();
		lbl_ch3.invalidate();
		// Ch4
		if(g1_rmm_buffer[3] == RMM_POOR){
			res_ch4Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 50, 50));
			res_ch4.setPainter(res_ch4Painter);
			lbl_ch4.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		else if(g1_rmm_buffer[3] == RMM_MODERATE){
			res_ch4Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 250, 50));
			res_ch4.setPainter(res_ch4Painter);
			lbl_ch4.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
		}
		else if(g1_rmm_buffer[3] == RMM_GOOD){
			res_ch4Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 175, 0));
			res_ch4.setPainter(res_ch4Painter);
			lbl_ch4.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		res_ch4.invalidate();
		lbl_ch4.invalidate();
		g1_update_rmm_flag = false;
	}
	// Group 2
	else if(g2_update_rmm_flag){
		// Ch5
		if(g2_rmm_buffer[0] == RMM_POOR){
			res_ch5Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 50, 50));
			res_ch5.setPainter(res_ch5Painter);
			lbl_ch5.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		else if(g2_rmm_buffer[0] == RMM_MODERATE){
			res_ch5Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 250, 50));
			res_ch5.setPainter(res_ch5Painter);
			lbl_ch5.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
		}
		else if(g2_rmm_buffer[0] == RMM_GOOD){
			res_ch5Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 175, 0));
			res_ch5.setPainter(res_ch5Painter);
			lbl_ch5.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		res_ch5.invalidate();
		lbl_ch5.invalidate();
		//Ch6
		if(g2_rmm_buffer[1] == RMM_POOR){
			res_ch6Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 50, 50));
			res_ch6.setPainter(res_ch6Painter);
			lbl_ch6.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		else if(g2_rmm_buffer[1] == RMM_MODERATE){
			res_ch6Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 250, 50));
			res_ch6.setPainter(res_ch6Painter);
			lbl_ch6.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
		}
		else if(g2_rmm_buffer[1] == RMM_GOOD){
			res_ch6Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 175, 0));
			res_ch6.setPainter(res_ch6Painter);
			lbl_ch6.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		res_ch6.invalidate();
		lbl_ch6.invalidate();
		// Ch7
		if(g2_rmm_buffer[2] == RMM_POOR){
			res_ch7Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 50, 50));
			res_ch7.setPainter(res_ch7Painter);
			lbl_ch7.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		else if(g2_rmm_buffer[2] == RMM_MODERATE){
			res_ch7Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 250, 50));
			res_ch7.setPainter(res_ch7Painter);
			lbl_ch7.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
		}
		else if(g2_rmm_buffer[2] == RMM_GOOD){
			res_ch7Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 175, 0));
			res_ch7.setPainter(res_ch7Painter);
			lbl_ch7.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		res_ch7.invalidate();
		lbl_ch7.invalidate();
		// Ch8
		if(g2_rmm_buffer[3] == RMM_POOR){
			res_ch8Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 50, 50));
			res_ch8.setPainter(res_ch8Painter);
			lbl_ch8.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		else if(g2_rmm_buffer[3] == RMM_MODERATE){
			res_ch8Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(250, 250, 50));
			res_ch8.setPainter(res_ch8Painter);
			lbl_ch8.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
		}
		else if(g2_rmm_buffer[3] == RMM_GOOD){
			res_ch8Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 175, 0));
			res_ch8.setPainter(res_ch8Painter);
			lbl_ch8.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
		}
		res_ch8.invalidate();
		lbl_ch8.invalidate();
		g2_update_rmm_flag = false;
	}
	else if(stim_update_scr_flag){
		if(stim_state == NO_STIM){			// NO STIM
			rmm_group1.setVisible(false);
			rmm_group1.invalidate();
			rmm_group2.setVisible(false);
			rmm_group2.invalidate();
			time_rem_disp.setVisible(false);
			time_rem_disp.invalidate();
		}
		else if(stim_state == SCAN_MODE){		// SCAN_MODE
			rmm_group1.setVisible(true);
			rmm_group1.invalidate();
			rmm_group2.setVisible(true);
			rmm_group2.invalidate();
			//time_rem_disp.setVisible(true);
			//time_rem_disp.invalidate();
		}
		else if(stim_state == STIM_MODE){	// STIM_MODE
			rmm_group1.setVisible(true);
			rmm_group1.invalidate();
			rmm_group2.setVisible(true);
			rmm_group2.invalidate();
			time_rem_disp.setVisible(true);
			time_rem_disp.invalidate();
		}
		stim_update_scr_flag = false;
	}
	else if(time_rem_update_flag){
		time_rem_min = ((time_remaining/4)/60);
		time_rem_sec = ((time_remaining/4)%60);
		Unicode::snprintf(ta_time_countdownBuffer, TA_TIME_COUNTDOWN_SIZE, "%02u:%02u",time_rem_min, time_rem_sec);
		ta_time_countdown.invalidate();
		time_rem_update_flag = false;
	}
	else if(update_battery_flag){
		Unicode::snprintf(ta_batt_levelBuffer, TA_BATT_LEVEL_SIZE, "%u",(unsigned int)batt_percentage);
		ta_batt_level.invalidate();
		update_battery_flag = false;
	}
	else if(update_title_flag){
		Unicode::strncpy(ta_titleBuffer, title_buffer, TA_TITLE_SIZE);
		ta_title.invalidate();
		update_title_flag = false;
		return;
	}
	else if(update_mf_line_flag[0]){
		Unicode::strncpy(ta_main_l1Buffer, mf_line_buffer[0], TA_MAIN_L1_SIZE);
		ta_main_l1.invalidate();
		update_mf_line_flag[0] = false;
	}
	else if(update_mf_line_flag[1]){
		Unicode::strncpy(ta_main_l2Buffer, mf_line_buffer[1], TA_MAIN_L2_SIZE);
		ta_main_l2.invalidate();
		update_mf_line_flag[1] = false;
	}
	else if(update_mf_line_flag[2]){
		Unicode::strncpy(ta_main_l3Buffer, mf_line_buffer[2], TA_MAIN_L3_SIZE);
		ta_main_l3.invalidate();
		update_mf_line_flag[2] = false;
	}
	else if(update_mf_line_flag[3]){
		Unicode::strncpy(ta_main_l4Buffer, mf_line_buffer[3], TA_MAIN_L4_SIZE);
		ta_main_l4.invalidate();
		update_mf_line_flag[3] = false;
	}
	else if(update_mf_line_flag[4]){
		Unicode::strncpy(ta_main_l5Buffer, mf_line_buffer[4], TA_MAIN_L5_SIZE);
		ta_main_l5.invalidate();
		update_mf_line_flag[4] = false;
	}
	else if(update_mf_line_flag[5]){
		Unicode::strncpy(ta_main_l6Buffer, mf_line_buffer[5], TA_MAIN_L6_SIZE);
		ta_main_l6.invalidate();
		update_mf_line_flag[5] = false;
	}
	else if(update_mf_line_flag[6]){
		Unicode::strncpy(ta_main_l7Buffer, mf_line_buffer[6], TA_MAIN_L7_SIZE);
		ta_main_l7.invalidate();
		update_mf_line_flag[6] = false;
	}
	else if(update_mf_line_flag[7]){
		Unicode::strncpy(ta_main_l8Buffer, mf_line_buffer[7], TA_MAIN_L8_SIZE);
		ta_main_l8.invalidate();
		update_mf_line_flag[7] = false;
	}
	else if(update_mf_line_flag[8]){
		Unicode::strncpy(ta_main_l9Buffer, mf_line_buffer[8], TA_MAIN_L9_SIZE);
		ta_main_l9.invalidate();
		update_mf_line_flag[8] = false;
	}
	else if(update_left_opt_flag){
		Unicode::strncpy(ta_leftoptionBuffer, left_opt_buffer, TA_LEFTOPTION_SIZE);
		ta_leftoption.invalidate();
		update_left_opt_flag = false;
	}
	else if(update_center_opt_flag){
		Unicode::strncpy(ta_centeroptionBuffer, center_opt_buffer, TA_CENTEROPTION_SIZE);
		ta_centeroption.invalidate();
		update_center_opt_flag = false;
	}
	else if(update_right_opt_flag){
		Unicode::strncpy(ta_rightoptionBuffer, right_opt_buffer, TA_RIGHTOPTION_SIZE);
		ta_rightoption.invalidate();
		update_right_opt_flag = false;
	}
}
