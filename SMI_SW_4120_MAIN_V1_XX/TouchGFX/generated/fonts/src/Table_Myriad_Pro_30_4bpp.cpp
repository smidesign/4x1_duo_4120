// Autogenerated, do not edit

#include <fonts/GeneratedFont.hpp>

FONT_TABLE_LOCATION_FLASH_PRAGMA
KEEP extern const touchgfx::GlyphNode glyphs_Myriad_Pro_30_4bpp[] FONT_TABLE_LOCATION_FLASH_ATTRIBUTE =
{
    {     0, 0x0020,   0,   0,   0,   0,   6,   0,   0, 0x00 },
    {     0, 0x0021,   5,  21,  21,   1,   7,   0,   0, 0x00 },
    {    63, 0x0022,   8,   8,  21,   1,  10,   0,   8, 0x00 },
    {    95, 0x0023,  13,  20,  20,   1,  15,   0,   0, 0x00 },
    {   235, 0x0024,  12,  25,  22,   2,  15,   0,   0, 0x00 },
    {   385, 0x0025,  23,  20,  20,   0,  24,   0,   0, 0x00 },
    {   625, 0x0026,  17,  21,  21,   1,  18,   0,   0, 0x00 },
    {   814, 0x0027,   4,   8,  21,   1,   6,   8,   8, 0x00 },
    {   830, 0x0028,   7,  25,  21,   1,   9,   0,   0, 0x00 },
    {   930, 0x0029,   7,  25,  21,   0,   9,  16,   8, 0x00 },
    {  1030, 0x002A,  12,  11,  21,   0,  12,   0,   0, 0x00 },
    {  1096, 0x002B,  16,  17,  17,   1,  18,   0,   0, 0x00 },
    {  1232, 0x002C,   6,   9,   4,   0,   6,  24,  21, 0x00 },
    {  1259, 0x002D,   9,   2,   9,   0,   9,  45,   8, 0x00 },
    {  1269, 0x002E,   4,   4,   4,   1,   6,  53,  21, 0x00 },
    {  1277, 0x002F,  11,  22,  21,   0,  10,   0,   0, 0x00 },
    {  1409, 0x0030,  14,  20,  20,   1,  15,   0,   0, 0x00 },
    {  1549, 0x0031,   7,  20,  20,   3,  15,   0,   0, 0x00 },
    {  1629, 0x0032,  13,  20,  20,   1,  15,   0,   0, 0x00 },
    {  1769, 0x0033,  13,  20,  20,   1,  15,   0,   0, 0x00 },
    {  1909, 0x0034,  15,  20,  20,   0,  15,   0,   0, 0x00 },
    {  2069, 0x0035,  13,  20,  20,   1,  15,   0,   0, 0x00 },
    {  2209, 0x0036,  14,  20,  20,   1,  15,   0,   0, 0x00 },
    {  2349, 0x0037,  13,  20,  20,   1,  15,   0,   0, 0x00 },
    {  2489, 0x0038,  14,  20,  20,   1,  15,   0,   0, 0x00 },
    {  2629, 0x0039,  14,  20,  20,   1,  15,   0,   0, 0x00 },
    {  2769, 0x003A,   4,  14,  14,   1,   6,  74,   7, 0x00 },
    {  2797, 0x003B,   6,  19,  14,   0,   6,  81,   7, 0x00 },
    {  2854, 0x003C,  14,  16,  16,   2,  18,   0,   0, 0x00 },
    {  2966, 0x003D,  16,   8,  12,   1,  18,   0,   0, 0x00 },
    {  3030, 0x003E,  14,  16,  16,   2,  18,   0,   0, 0x00 },
    {  3142, 0x003F,  10,  21,  21,   1,  12,   0,   0, 0x00 },
    {  3247, 0x0040,  20,  21,  18,   1,  22,   0,   0, 0x00 },
    {  3457, 0x0041,  18,  21,  21,   0,  18,  88,   9, 0x00 },
    {  3646, 0x0042,  13,  21,  21,   2,  16,   0,   0, 0x00 },
    {  3793, 0x0043,  16,  21,  21,   1,  17,  97,   7, 0x00 },
    {  3961, 0x0044,  17,  21,  21,   2,  20,   0,   0, 0x00 },
    {  4150, 0x0045,  12,  21,  21,   2,  15,   0,   0, 0x00 },
    {  4276, 0x0046,  12,  21,  21,   2,  15,   0,   0, 0x00 },
    {  4402, 0x0047,  17,  21,  21,   1,  19, 104,   7, 0x00 },
    {  4591, 0x0048,  16,  21,  21,   2,  20,   0,   0, 0x00 },
    {  4759, 0x0049,   3,  21,  21,   2,   7,   0,   0, 0x00 },
    {  4801, 0x004A,   9,  21,  21,   0,  11, 111,  15, 0x00 },
    {  4906, 0x004B,  15,  21,  21,   2,  16,   0,   0, 0x00 },
    {  5074, 0x004C,  12,  21,  21,   2,  14,   0,   0, 0x00 },
    {  5200, 0x004D,  22,  21,  21,   1,  24, 126,   1, 0x00 },
    {  5431, 0x004E,  16,  21,  21,   2,  20,   0,   0, 0x00 },
    {  5599, 0x004F,  19,  21,  21,   1,  21, 127,   7, 0x00 },
    {  5809, 0x0050,  13,  21,  21,   2,  16,   0,   0, 0x00 },
    {  5956, 0x0051,  19,  24,  21,   1,  21, 134,   7, 0x00 },
    {  6196, 0x0052,  14,  21,  21,   2,  16,   0,   0, 0x00 },
    {  6343, 0x0053,  13,  21,  21,   1,  15,   0,   0, 0x00 },
    {  6490, 0x0054,  15,  21,  21,   0,  15, 141,  35, 0x00 },
    {  6658, 0x0055,  16,  21,  21,   2,  19, 176,   2, 0x00 },
    {  6826, 0x0056,  17,  21,  21,   0,  17, 178,  10, 0x00 },
    {  7015, 0x0057,  25,  21,  21,   0,  25, 188,  10, 0x00 },
    {  7288, 0x0058,  17,  21,  21,   0,  17, 198,   5, 0x00 },
    {  7477, 0x0059,  16,  21,  21,   0,  16, 203,  11, 0x00 },
    {  7645, 0x005A,  16,  21,  21,   0,  17, 214,   2, 0x00 },
    {  7813, 0x005B,   6,  25,  21,   2,   9,   0,   0, 0x00 },
    {  7888, 0x005C,  11,  22,  21,   0,  10,   0,   0, 0x00 },
    {  8020, 0x005D,   7,  25,  21,   0,   9, 216,   8, 0x00 },
    {  8120, 0x005E,  15,  14,  20,   1,  18,   0,   0, 0x00 },
    {  8232, 0x005F,  15,   2, 254,   0,  15,   0,   0, 0x60 },
    {  8248, 0x0060,   6,   5,  21,   0,   9,   0,   0, 0x00 },
    {  8263, 0x0061,  12,  15,  15,   1,  14, 224,   7, 0x00 },
    {  8353, 0x0062,  14,  22,  22,   2,  17, 231,   2, 0x00 },
    {  8507, 0x0063,  12,  15,  15,   1,  13, 233,   7, 0x00 },
    {  8597, 0x0064,  14,  22,  22,   1,  17, 240,   7, 0x00 },
    {  8751, 0x0065,  13,  15,  15,   1,  15, 247,   7, 0x00 },
    {  8856, 0x0066,  10,  22,  22,   0,   9, 254,   3, 0x00 },
    {  8966, 0x0067,  14,  21,  15,   1,  17,   1,   3, 0x01 },
    {  9113, 0x0068,  13,  22,  22,   2,  17,   4,   2, 0x01 },
    {  9267, 0x0069,   5,  21,  21,   1,   7,   6,   3, 0x01 },
    {  9330, 0x006A,   8,  27,  21,  -2,   7,   9,   3, 0x01 },
    {  9438, 0x006B,  13,  22,  22,   2,  14,  12,   2, 0x01 },
    {  9592, 0x006C,   3,  22,  22,   2,   7,  14,   2, 0x01 },
    {  9636, 0x006D,  21,  15,  15,   2,  25,  16,   3, 0x01 },
    {  9801, 0x006E,  13,  15,  15,   2,  17,  19,   3, 0x01 },
    {  9906, 0x006F,  15,  15,  15,   1,  16,  22,   7, 0x01 },
    { 10026, 0x0070,  14,  21,  15,   2,  17,  29,   3, 0x01 },
    { 10173, 0x0071,  14,  21,  15,   1,  17,  32,   7, 0x01 },
    { 10320, 0x0072,   8,  15,  15,   2,  10,  39,   3, 0x01 },
    { 10380, 0x0073,  10,  15,  15,   1,  12,  42,   5, 0x01 },
    { 10455, 0x0074,  10,  19,  19,   0,  10,  47,   8, 0x01 },
    { 10550, 0x0075,  13,  15,  15,   2,  17,  55,   5, 0x01 },
    { 10655, 0x0076,  14,  15,  15,   0,  14,  60,  12, 0x01 },
    { 10760, 0x0077,  22,  15,  15,   0,  22,  72,  12, 0x01 },
    { 10925, 0x0078,  14,  15,  15,   0,  14,  84,   3, 0x01 },
    { 11030, 0x0079,  14,  23,  15,   0,  14,  87,  12, 0x01 },
    { 11191, 0x007A,  13,  15,  15,   0,  13,  99,   2, 0x01 },
    { 11296, 0x007B,   8,  25,  21,   0,   9,   0,   0, 0x00 },
    { 11396, 0x007C,   3,  30,  22,   2,   7,   0,   0, 0x00 },
    { 11456, 0x007D,   8,  25,  21,   0,   9, 101,   8, 0x01 },
    { 11556, 0x007E,  16,   5,  11,   1,  18,   0,   0, 0x00 }
};

// Myriad_Pro_30_4bpp
extern const touchgfx::GlyphNode glyphs_Myriad_Pro_30_4bpp[];
extern const uint8_t unicodes_Myriad_Pro_30_4bpp_0[];
extern const uint8_t* const unicodes_Myriad_Pro_30_4bpp[] =
{
    unicodes_Myriad_Pro_30_4bpp_0
};
extern const touchgfx::KerningNode kerning_Myriad_Pro_30_4bpp[];

touchgfx::GeneratedFont& getFont_Myriad_Pro_30_4bpp();

touchgfx::GeneratedFont& getFont_Myriad_Pro_30_4bpp()
{
    static touchgfx::GeneratedFont Myriad_Pro_30_4bpp(glyphs_Myriad_Pro_30_4bpp, 95, 30, 8, 4, 1, 2, 1, unicodes_Myriad_Pro_30_4bpp, kerning_Myriad_Pro_30_4bpp, 63, 0, 0);
    return Myriad_Pro_30_4bpp;
}
