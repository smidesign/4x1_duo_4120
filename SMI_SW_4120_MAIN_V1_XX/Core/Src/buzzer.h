/*
 * buzzer.h
 *
 *  Created on: Mar 14, 2021
 *      Author: SMI_DESIGN-05
 */

#ifndef SRC_BUZZER_H_
#define SRC_BUZZER_H_

#define NO_BEEP 	0
#define SHORT_BEEP 	1
#define MED_BEEP	2
#define LONG_BEEP	3
#define CHIRP		4

void app_buzzer(void);

#endif /* SRC_BUZZER_H_ */
