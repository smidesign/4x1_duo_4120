/*
 * keypad.h
 *
 *  Created on: Feb 9, 2021
 *      Author: SMI_DESIGN-05
 */

#ifndef SRC_KEYPAD_H_
#define SRC_KEYPAD_H_

#include <stdbool.h>
#include "main.h"

#define KEYPAD_NO_KEY	0xFF
#define KEYPAD_BACK		0xFFFFAAAA


// Macros
#define COL1_HIGH	HAL_GPIO_WritePin(COL1_GPIO_Port, COL1_Pin, GPIO_PIN_SET)
#define COL1_LOW	HAL_GPIO_WritePin(COL1_GPIO_Port, COL1_Pin, GPIO_PIN_RESET)

#define COL2_HIGH	HAL_GPIO_WritePin(COL2_GPIO_Port, COL2_Pin, GPIO_PIN_SET)
#define COL2_LOW	HAL_GPIO_WritePin(COL2_GPIO_Port, COL2_Pin, GPIO_PIN_RESET)

#define COL3_HIGH	HAL_GPIO_WritePin(COL3_GPIO_Port, COL3_Pin, GPIO_PIN_SET)
#define COL3_LOW	HAL_GPIO_WritePin(COL3_GPIO_Port, COL3_Pin, GPIO_PIN_RESET)

// Functions
//void init_keypad(void);	// Pins initialized by CUBEMX
char get_key(void);
void wait_for_key(void);

void app_keypad(void);

#endif /* SRC_KEYPAD_H_ */
