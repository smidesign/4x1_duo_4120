/*
 * buzzer.c
 *
 *  Created on: Mar 14, 2021
 *      Author: SMI_DESIGN-05
 */
#include "main.h"
#include "buzzer.h"

extern TIM_HandleTypeDef htim14;
uint8_t beep_length;
uint32_t app_buzzer_counter = 0;

void app_buzzer(void){
	if(beep_length == SHORT_BEEP){
		if(app_buzzer_counter == 0){
			HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
			app_buzzer_counter++;
		}
		else if(app_buzzer_counter < 2){
			app_buzzer_counter++;
		}
		else{
			HAL_TIM_PWM_Stop(&htim14, TIM_CHANNEL_1);
			beep_length = NO_BEEP;
			app_buzzer_counter = 0;
		}
	}
	else if(beep_length == MED_BEEP){
		if(app_buzzer_counter == 0){
			HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
			app_buzzer_counter++;
		}
		else if(app_buzzer_counter < 6){
			app_buzzer_counter++;
		}
		else{
			HAL_TIM_PWM_Stop(&htim14, TIM_CHANNEL_1);
			beep_length = NO_BEEP;
			app_buzzer_counter = 0;
		}
	}
	else if(beep_length == LONG_BEEP){
		if(app_buzzer_counter == 0){
			HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
			app_buzzer_counter++;
		}
		else if(app_buzzer_counter < 40){
			app_buzzer_counter++;
		}
		else{
			HAL_TIM_PWM_Stop(&htim14, TIM_CHANNEL_1);
			beep_length = NO_BEEP;
			app_buzzer_counter = 0;
		}
	}
	else if(beep_length == CHIRP){
		if(app_buzzer_counter == 0){
			HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
			app_buzzer_counter++;
		}
		else if(app_buzzer_counter < 2){
			app_buzzer_counter++;
		}
		else if(app_buzzer_counter == 2){
			HAL_TIM_PWM_Stop(&htim14, TIM_CHANNEL_1);
			app_buzzer_counter++;
		}
		else if(app_buzzer_counter < 5){
			app_buzzer_counter++;
		}
		else if(app_buzzer_counter == 5){
			HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
			app_buzzer_counter++;
		}
		else if(app_buzzer_counter < 7){
			app_buzzer_counter++;
		}
		else{
			HAL_TIM_PWM_Stop(&htim14, TIM_CHANNEL_1);
			beep_length = NO_BEEP;
			app_buzzer_counter = 0;
		}
	}
}

