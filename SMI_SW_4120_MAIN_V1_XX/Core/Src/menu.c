/*
 * menu.c
 *
 *  Created on: Mar 7, 2021
 *      Author: SMI_DESIGN-05
 */

#include "menu.h"
#include "device_configs.h"
#include "commands.h"

// Screen update flags
bool update_battery_flag;
bool update_title_flag;
bool update_mf_line_flag[9];
bool update_left_opt_flag, update_center_opt_flag, update_right_opt_flag;
bool display_splash_msg_flag, hide_splash_msg_flag, splash_msg_active_flag;

// Buffers
char title_buffer[20];
char left_opt_buffer[20];
char center_opt_buffer[20];
char right_opt_buffer[20];
char mf_line_buffer[9][100];
char sm_buffer[100], sm_title_buffer[20];

extern char keypress;
extern bool keypress_flag;
extern uint8_t beep_length;

extern bool eep_read_in_progress, eep_write_in_progress;
extern uint32_t eep_mem_read_data;
extern uint32_t adc_buffer[1];
uint32_t wait_timer = 0;
bool stim_update_scr_flag;

uint32_t g1_intensity, g1_polarity, g2_intensity, g2_polarity, duration, sham;
char str_g1_intensity[50], str_g2_intensity[50], str_duration[25], str_sham[10];
char str_stim_code[25], str_critical_events[25], str_critical_time[25], str_total_stim_time[30];
uint32_t code_bank[MAX_CODES], temp_code_bank[MAX_CODES];
uint8_t code_use_flag[MAX_CODES];
bool code_gen_en_flag, code_gen_complete_flag;
extern uint32_t tim7_counter, tim10_counter, tim11_counter;
bool read_codes_from_mem_flag = false, codes_read_complete_flag;
uint32_t adc_accum = 0, adc_count = 0, adc_batt_avg = 0;
float batt_voltage = 0.0f;
unsigned int batt_percentage = 100;
uint32_t sm_wait_time;

uint32_t stim_state;
uint32_t temp_idx;
bool code_found_flag, code_used_flag;
uint32_t code_idx;
bool time_rem_update_flag;
uint32_t time_remaining, stim_time_s, duration_s;

extern UART_HandleTypeDef huart4;
extern UART_HandleTypeDef huart5;
extern uint8_t uart4_tx_buffer[UART_COM_SIZE];
bool g1_update_rmm_flag, g2_update_rmm_flag;
bool stimulation_active_flag = false, end_of_stim_flag;

uint8_t g1_rmm_buffer[4], prev_g1_rmm_buffer[4];
uint8_t g2_rmm_buffer[4], prev_g2_rmm_buffer[4];
uint32_t unique_critical_count, total_time_in_critical;
bool abort_flag = false;

uint32_t temp_critical_events, temp_critical_time, temp_stim_time;
uint32_t temp_summary_code;
char str_summary_code[15];
uint32_t shutdown_counter = 0, rmm_update_counter = 0;

/*
 *
 */
 void print_menu_items(const char *prompt, ...){
	if(strlen(prompt)==0){	// Empty line
		return;
	}
	unsigned int line_idx = 0;
	//Print the prompt
	strncpy(mf_line_buffer[line_idx], prompt, 100);
	update_mf_line_flag[line_idx++] = true;
	//Create and start the VA list
	va_list menuItems;
	va_start(menuItems, prompt);
	//Print all menu items
	char *menuItem = va_arg(menuItems, char *);
	while(menuItem[0] != '\n')	//Add send argument to check end of page
	{
		strncpy(mf_line_buffer[line_idx], menuItem, 100);
		update_mf_line_flag[line_idx++] = true;
		menuItem = va_arg(menuItems, char *);
	}
	while(line_idx < 9){
		strncpy(mf_line_buffer[line_idx], " ", 100);
		update_mf_line_flag[line_idx++] = true;;
	}
	va_end(menuItems);
}
void print_title(const char *title){
	strncpy(title_buffer, title, 20);
	update_title_flag = true;
}
void print_options(const char *left_option, const char *center_option, const char *right_option){
	strncpy(left_opt_buffer, left_option, 20);
	update_left_opt_flag = true;
	strncpy(center_opt_buffer, center_option, 20);
	update_center_opt_flag = true;
	strncpy(right_opt_buffer, right_option, 20);
	update_right_opt_flag = true;
}
void display_splash_msg(const char *title, const char *msg, uint32_t wait_time){
	strncpy(sm_title_buffer, title, 20);
	strncpy(sm_buffer, msg, 100);
	wait_timer = 0;
	sm_wait_time = wait_time;
	splash_msg_active_flag = true;
}

uint32_t in_line_num, num_in;
char key_pressed_buff[10];
uint8_t key_index = 0;
bool num_entered_flag = false;

static void get_number(){
	if(splash_msg_active_flag){ return; }
	if(key_index == 0){
		print_options("* BACK", "", "OK #");
	}
	else{
		print_options("* BACKSPACE", "", "OK #");
	}

	if(keypress_flag){
		if(keypress >= '0' && keypress <= '9' && key_index < 10){
			key_pressed_buff[key_index++] = keypress;
		}
		else if(keypress == '*'){
			if(key_index == 0){		// back pressed
				num_in = KEYPAD_BACK;
				num_entered_flag = true;
			}
			else{					// backspace pressed
				key_pressed_buff[--key_index] = '\0';
			}
		}
		else if(keypress == '#'){
			if(key_index == 0){
				num_in = 0;
				num_entered_flag = true;
			}
			else if(key_index < 10){
				key_pressed_buff[key_index] = '\0';
			}
			num_in = strtoul((const char*)key_pressed_buff, NULL, 10);
			memset(key_pressed_buff, '\0', 10);
			key_index = 0;
			num_entered_flag = true;
		}
		keypress_flag = false;
		strncpy(mf_line_buffer[in_line_num-1], key_pressed_buff, 10);
		update_mf_line_flag[in_line_num-1] = true;
	}
}

enum devicestate future_device_state;
enum menustate future_menu_state;
void app_splash_msg(void){
	if(splash_msg_active_flag){
		if(wait_timer == 0){
			display_splash_msg_flag = true;
			wait_timer++;
		}
		else if(wait_timer < sm_wait_time){
			wait_timer++;
			if(keypress_flag == true){
				wait_timer = sm_wait_time;
				wait_for_key();
			}
		}
		else if(wait_timer >= sm_wait_time){
			hide_splash_msg_flag = true;
			device_state = future_device_state;
			menu_state = future_menu_state;
			keypress_flag = false;
			splash_msg_active_flag = false;
		}
	}
}

void app_menu(void){
	/*
	 * Main Menu
	 */
	if(device_state == MAIN_MENU){
		if(menu_state == MENU_UPDATE_DISP){
			print_title("MAIN MENU");
			print_menu_items("Select an option:",
							"",
							"1. STIMULATION",
							"2. ADMINISTRATION",
							"3. SUMMARY CODE",
							"4. SHUTDOWN",
							"\n");
			print_options(" ", " ", " ");
			menu_state = MENU_WFI;
			wait_for_key();
		}
		else if(menu_state == MENU_WFI){
			if(keypress_flag){
				if(keypress == '1'){
					eeprom_read_dword(EEMEM_STIM_SETTINGS_SETUP);
					device_state = STIM_MENU;
					menu_state = MENU_CHECK_LOW_BATT;

				}
				else if(keypress == '2'){
					device_state = ADMIN_MENU_AUTH;
					menu_state = MENU_UPDATE_DISP;
				}
				else if(keypress == '3'){
					eeprom_read_dword(EEMEM_SUMMARY_CODE);
					device_state = SUMMARY_CODE;
					menu_state = MENU_EEMEM_WFD;
				}
				else if(keypress == '4'){
					shutdown_counter = 0;
					device_state = SHUTDOWN;
				}
				wait_for_key();
			}
		}
	}
	/*
	 * Stim menu
	 */
	else if(device_state == STIM_MENU){
		if(menu_state == MENU_CHECK_LOW_BATT){
			if(batt_percentage < 5.00f){
				display_splash_msg("WARNING", "LOW BATTERY!", 20);
				future_device_state = MAIN_MENU;
				future_menu_state = MENU_UPDATE_DISP;
				menu_state = MENU_NOP;
			}
			else{
				menu_state = MENU_EEMEM_WFD;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD){
			/* TEST DATA BEGIN*/
			/*
			g1_intensity = TEST_G1_INTENSITY;
			send_uart4(SET_STIM_INTENSITY, g1_intensity);
			send_uart5(SET_STIM_INTENSITY, g2_intensity);

			// TEST DATA

			g1_polarity = TEST_G1_POLARITY;
			g2_intensity = TEST_G2_INTENSITY;
			g2_polarity = TEST_G2_POLARITY;
			duration = TEST_DURATION;
			duration_s = duration*60;
			time_remaining = duration*60*4;
			stim_time_s = 0;
			sham = TEST_SHAM;
			menu_state = MENU_UPDATE_DISP3;
			return;
			*/
			/* TEST DATA END */
			if(!eep_read_in_progress){
				if(eep_mem_read_data == STIM_SETTINGS_SET_FLAG){
					eeprom_read_dword(EEMEM_INTENSITY_G1);			//Read group 1 intensity
					menu_state = MENU_EEMEM_WFD1;
				}
				else{
					display_splash_msg("WARNING", "NOT READY TO\nSTIMULATE.\nCONTACT ADMINISTRATOR!", 20);
					future_device_state = MAIN_MENU;
					menu_state = MENU_UPDATE_DISP;
					menu_state = MENU_NOP;
				}
			}
		}
		else if(menu_state == MENU_NOP){

		}
		else if(menu_state == MENU_EEMEM_WFD1){
			if(!eep_read_in_progress){
				g1_intensity = eep_mem_read_data;
				eeprom_read_dword(EEMEM_POLARITY_G1);			//Read group 1 polarity
				menu_state = MENU_EEMEM_WFD2;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD2){
			if(!eep_read_in_progress){
				g1_polarity = eep_mem_read_data;
				eeprom_read_dword(EEMEM_INTENSITY_G2);			//Read group 2 intensity
				menu_state = MENU_EEMEM_WFD3;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD3){
			if(!eep_read_in_progress){
				g2_intensity = eep_mem_read_data;
				eeprom_read_dword(EEMEM_POLARITY_G2);			//Read group 2 polarity
				menu_state = MENU_EEMEM_WFD4;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD4){
			if(!eep_read_in_progress){
				g2_polarity = eep_mem_read_data;
				eeprom_read_dword(EEMEM_DURATION);				//Read duration
				menu_state = MENU_EEMEM_WFD5;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD5){
			if(!eep_read_in_progress){
				duration = eep_mem_read_data;
				duration_s = duration*60;
				time_remaining = duration*60*4;
				stim_time_s = 0;
				eeprom_read_dword(EEMEM_SHAM);					//Read SHAM
				menu_state = MENU_EEMEM_WFD6;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD6){
			if(!eep_read_in_progress){
				sham = eep_mem_read_data;
				eeprom_read_dword(EEMEM_CODES_GENERATED);
				menu_state = MENU_EEMEM_WFD7;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD7){
			if(!eep_read_in_progress){
				if(eep_mem_read_data == CODE_GEN_FLAG){	// Codes have been generated
					display_splash_msg("INFO", "PLEASE WAIT...", 20);
					read_codes_from_mem_flag = true;
					codes_read_complete_flag = false;
					future_device_state = STIM_MENU;
					future_menu_state = MENU_UPDATE_DISP1;
					menu_state = MENU_NOP;
				}
				else{
					display_splash_msg("WARNING", "CODES NOT GENERATED!", 20);
					future_device_state = MAIN_MENU;
					future_menu_state = MENU_UPDATE_DISP;
					menu_state = MENU_NOP;
				}
			}
		}
		else if(menu_state == MENU_UPDATE_DISP1){
			print_title("STIMULATION");
			print_menu_items("Please put on your headgear and HD electrodes as instructed","\n");
			print_options("* BACK", "", "STIMULATE #");
			stim_state = SCAN_MODE;
			send_uart4(SET_STIM_STATUS, SCAN_MODE);
			send_uart5(SET_STIM_STATUS, SCAN_MODE);
			stim_update_scr_flag = true;
			menu_state = MENU_WFI;
			g1_update_rmm_flag = true;
			g2_update_rmm_flag = true;
			wait_for_key();
		}
		else if(menu_state == MENU_WFI){				// Get key press
			if(keypress_flag){
				if(keypress == '*'){
					device_state = MAIN_MENU;
					menu_state = MENU_UPDATE_DISP;
					stim_state = NO_STIM;
					stim_update_scr_flag = true;
				}
				else if(keypress == '#'){
					menu_state = MENU_UPDATE_DISP2;
					return;
					// Make sure no contact quality is poor.
					if(g1_rmm_buffer[0] == RMM_POOR || g1_rmm_buffer[1] == RMM_POOR || g1_rmm_buffer[2] == RMM_POOR || g1_rmm_buffer[3] == RMM_POOR ||
					   g2_rmm_buffer[0] == RMM_POOR || g2_rmm_buffer[1] == RMM_POOR || g2_rmm_buffer[2] == RMM_POOR || g2_rmm_buffer[3] == RMM_POOR){
						display_splash_msg("WARNING", "POOR CONTACT QUALITY!\nADJUST HEADGEAR AND\nHD ELECTRODES TO\nPROCEED", 20);
						future_device_state = STIM_MENU;
						future_menu_state = MENU_UPDATE_DISP1;
						menu_state = MENU_NOP;
					}
					else{
						menu_state = MENU_UPDATE_DISP2;
					}
				}
				wait_for_key();
			}
			else{
				if(!splash_msg_active_flag){
					rmm_update_counter++;
					if(rmm_update_counter >= 4){
						g1_update_rmm_flag = true;
						g2_update_rmm_flag = true;
						rmm_update_counter = 0;
					}

				}
			}
		}
		else if(menu_state == MENU_UPDATE_DISP2){		// Setup window to get STIM code
			stim_state = NO_STIM;						// Hide resistance measurement
			stim_update_scr_flag = true;
			print_title("CODE VERIFICATION");
			print_menu_items("Enter DOSE CODE:", "\n");
			menu_state = MENU_WFI1;
			wait_for_key();
			in_line_num = 2;
		}
		else if(menu_state == MENU_WFI1){				// Get STIM code
			get_number();
			if(num_entered_flag){
				// if number is valid continue to stimulation
				if(num_in == KEYPAD_BACK){
					device_state = MAIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				else{
					menu_state = MENU_GP0;
				}
				num_entered_flag = false;
			}
		}
		else if(menu_state == MENU_GP0){			// Verify code here
			for(temp_idx = 0; temp_idx < MAX_CODES; temp_idx++){
				if(code_bank[temp_idx] == num_in){	// Code found
					break;
				}
			}
			menu_state = MENU_GP1;
		}
		else if(menu_state == MENU_GP1){
			if(temp_idx >= MAX_CODES){
				display_splash_msg("WARNING", "INVALID CODE!", 20);
				future_device_state = STIM_MENU;
				future_menu_state = MENU_UPDATE_DISP2;
				menu_state = MENU_NOP;
			}
			else{
				if(code_use_flag[temp_idx] == 0xAA){
					display_splash_msg("WARNING", "CODE ALREADY USED!", 20);
					future_device_state = STIM_MENU;
					future_menu_state = MENU_UPDATE_DISP2;
					menu_state = MENU_NOP;
				}
				else{
					// Code is valid and unused
					code_idx = temp_idx;		// Save the code index
					eeprom_write_dword(EEMEM_CODES_ARRAY+(code_idx*4), (0xAA000000|code_bank[code_idx]));	// Mark code as used
					send_uart4(SET_STIM_INTENSITY, g1_intensity);
					send_uart5(SET_STIM_INTENSITY, g2_intensity);
					print_menu_items(" ", "\n");
					print_options("", "", "");
					display_splash_msg("INFO", "READY TO STIMULATE!\nPRESS ANY KEY TO\nCONTINUE", 2400);
					future_device_state = STIM_MENU;
					future_menu_state = MENU_UPDATE_DISP3;
					menu_state = MENU_NOP;
				}
			}
		}
		else if(menu_state == MENU_UPDATE_DISP3){
			print_title("STIMULATION");
			send_uart4(SET_POLARITY, g1_polarity);
			send_uart5(SET_POLARITY, g2_polarity);
			stim_state = STIM_MODE;
			stim_update_scr_flag = true;
			print_options("","PRESS '0' TO ABORT","");
			menu_state = MENU_UART_SEND1;
			wait_for_key();
		}
		else if(menu_state == MENU_UART_SEND1){
			stimulation_active_flag = true;
			send_uart4(RAMP_UP, NO_DATA);
			send_uart5(RAMP_UP, NO_DATA);
			menu_state = MENU_UART_SEND2;
			wait_for_key();
			unique_critical_count = 0;
			total_time_in_critical = 0;
		}
		else if(menu_state == MENU_UART_SEND2){
			send_uart4(SET_STIM_STATUS, STIM_MODE);
			send_uart5(SET_STIM_STATUS, STIM_MODE);
			menu_state = MENU_WFI2;
		}
		else if(menu_state == MENU_WFI2){				// Stimulation
			// Stay here
			// time_remaining > 0
			// Abort is not triggered
			if(time_remaining > 0){
				time_remaining--;
				if(time_remaining%4 == 0){
					stim_time_s++;
					time_rem_update_flag = true;
					// Check for open circuit
					if(g1_rmm_buffer[0] == RMM_POOR || g1_rmm_buffer[1] == RMM_POOR || g1_rmm_buffer[2] == RMM_POOR || g1_rmm_buffer[3] == RMM_POOR ||
					   g2_rmm_buffer[0] == RMM_POOR || g2_rmm_buffer[1] == RMM_POOR || g2_rmm_buffer[2] == RMM_POOR || g2_rmm_buffer[3] == RMM_POOR ){
						beep_length = CHIRP;
						if(prev_g1_rmm_buffer[0]!=RMM_POOR && prev_g1_rmm_buffer[1]!=RMM_POOR &&  prev_g1_rmm_buffer[2]!=RMM_POOR && prev_g1_rmm_buffer[3]!=RMM_POOR &&
						   prev_g2_rmm_buffer[0]!=RMM_POOR && prev_g2_rmm_buffer[1]!=RMM_POOR &&  prev_g2_rmm_buffer[2]!=RMM_POOR && prev_g2_rmm_buffer[3]!=RMM_POOR ){
							unique_critical_count++;
						}
						total_time_in_critical++;
					}
					prev_g1_rmm_buffer[0] = g1_rmm_buffer[0];
					prev_g1_rmm_buffer[1] = g1_rmm_buffer[1];
					prev_g1_rmm_buffer[2] = g1_rmm_buffer[2];
					prev_g1_rmm_buffer[3] = g1_rmm_buffer[3];

					prev_g2_rmm_buffer[0] = g2_rmm_buffer[0];
					prev_g2_rmm_buffer[1] = g2_rmm_buffer[1];
					prev_g2_rmm_buffer[2] = g2_rmm_buffer[2];
					prev_g2_rmm_buffer[3] = g2_rmm_buffer[3];

					g1_update_rmm_flag = true;
					g2_update_rmm_flag = true;
					// ********** STIM RAMP CONTROL **********
					if(sham == STIM_SHAM_OFF){
						// Active stimulation
						if(stim_time_s == (duration_s-30)){
							send_uart4(RAMP_DOWN, NO_DATA);
							send_uart5(RAMP_DOWN, NO_DATA);
						}
					}
					else if(sham == STIM_SHAM_ON){
						if(stim_time_s == 30){	//sham ramp down
							send_uart4(RAMP_DOWN, NO_DATA);
							send_uart5(RAMP_DOWN, NO_DATA);
						}
						else if(stim_time_s == 60){	// Switch to scan RMM during SHAM stim
							send_uart4(SET_STIM_STATUS, SCAN_MODE);
							send_uart5(SET_STIM_STATUS, SCAN_MODE);
						}
						else if(stim_time_s == (duration_s-61)){
							send_uart4(SET_STIM_STATUS, STIM_MODE);	// Switch to STIM RMM during ramps
							send_uart5(SET_STIM_STATUS, STIM_MODE);	// Switch to STIM RMM during ramps
						}
						else if(stim_time_s == (duration_s-60)){
							send_uart4(RAMP_UP, NO_DATA);
							send_uart5(RAMP_UP, NO_DATA);
						}
						else if(stim_time_s == (duration_s-30)){
							send_uart4(RAMP_DOWN, NO_DATA);
							send_uart5(RAMP_DOWN, NO_DATA);
						}
					}
				}
			}
			else{
				menu_state = MENU_WFI3;
			}

			if(keypress_flag == true && abort_flag == false){
				if(keypress == '0'){
					abort_flag = true;
					// Abort has been pressed
					if(sham == STIM_SHAM_OFF){	// Active stimulation
						if(stim_time_s < 30){
							time_remaining = (stim_time_s*4);
							send_uart4(ABORT_RAMP_DOWN, NO_DATA);
							send_uart5(ABORT_RAMP_DOWN, NO_DATA);
						}
						else if(stim_time_s < (duration_s-30)){
							time_remaining = 120;
							send_uart4(RAMP_DOWN, NO_DATA);
							send_uart5(RAMP_DOWN, NO_DATA);
						}
						else{
							// Nothing to do already ramping down
						}
					}
					else{		// SHAM stimulation
						if(stim_time_s < 30){
							time_remaining = (stim_time_s*4);
							send_uart4(ABORT_RAMP_DOWN, NO_DATA);
							send_uart5(ABORT_RAMP_DOWN, NO_DATA);
						}
						else if(stim_time_s < 60){
							time_remaining = (60-stim_time_s)*4;
							// No command to send, already in ramp down
						}
						else if(stim_time_s < (duration_s-60)){
							time_remaining = 120;
							// No command to send since there is no ramp
						}
						else if(stim_time_s < (duration_s - 30)){
							time_remaining = (stim_time_s-(duration_s-60))*4;
							send_uart4(ABORT_RAMP_DOWN, NO_DATA);
							send_uart5(ABORT_RAMP_DOWN, NO_DATA);
						}
						else{
							// Nothing to do already ramping down
						}
					}	// else {
				} // if(keypress == '0')
				else{
					wait_for_key();
				}
			}// if(keypress_flag)
		}
		else if(menu_state == MENU_WFI3){
			beep_length = LONG_BEEP;
			stim_state = NO_STIM; // Hide resistance measurement
			stim_update_scr_flag = true;
			print_menu_items(" ","\n");
			menu_state = MENU_EEMEM_WRITE1;
		}
		else if(menu_state == MENU_EEMEM_WRITE1){
			if(!eep_write_in_progress){
				eeprom_write_dword(EEMEM_UNIQUE_CRITICAL_ARRAY+(code_idx*4), unique_critical_count);
				menu_state = MENU_EEMEM_WRITE2;
			}
		}
		else if(menu_state == MENU_EEMEM_WRITE2){
			if(!eep_write_in_progress){
				eeprom_write_dword(EEMEM_CRITICAL_DURATION_ARRAY+(code_idx*4), total_time_in_critical);
				menu_state = MENU_EEMEM_WRITE3;
			}
		}
		else if(menu_state == MENU_EEMEM_WRITE3){
			if(!eep_write_in_progress){
				eeprom_write_dword(EEMEM_STIM_DURATION_ARRAY+(code_idx*4), stim_time_s);
				menu_state = MENU_EEMEM_WRITE4;
			}
		}
		else if(menu_state == MENU_EEMEM_WRITE4){
			if(!eep_write_in_progress){
				uint32_t temp_critical_count, temp_time_in_critical;
				if(unique_critical_count > 99){
					temp_critical_count = 99;
				}
				else{
					temp_critical_count = unique_critical_count;
				}

				if(total_time_in_critical > 99){
					temp_time_in_critical = 99;
				}
				else{
					temp_time_in_critical = total_time_in_critical;
				}
				uint32_t summary_code = (((code_idx/9)+1)*100000000UL)
										+ (temp_critical_count*1000000UL)
										+ (temp_time_in_critical*10000UL)
										+ ((stim_time_s/60)*100)
										+ (code_idx%9)+1;
				eeprom_write_dword(EEMEM_SUMMARY_CODE, summary_code);
				menu_state = MENU_WFI4;
			}
		}
		else if(menu_state == MENU_WFI4){
			print_options("","","");
			display_splash_msg("INFO", "STIMULATION COMPLETE!\nREMOVE ELECTRODES AND\nPRESS ANY KEY TO\nSHUTDOWN", 2400);
			future_device_state = STIM_MENU;
			future_menu_state = MENU_WFI5;
			menu_state = MENU_NOP;
			wait_for_key();
		}
		else if(menu_state == MENU_WFI5){
			shutdown_counter = 0;
			device_state = SHUTDOWN;
		}
		else if(menu_state == MENU_NOP){
		}
	}
	/*
	 * Administration menu authentication
	 */
	else if(device_state == ADMIN_MENU_AUTH){
		static uint32_t saved_admin_pin = 0;
		if(menu_state == MENU_UPDATE_DISP){
			// Read saved admin pin
			eeprom_read_dword(EEMEM_ADMIN_PIN);
			print_title("ADMINISTRATION");
			print_menu_items("ADMINISTRATOR PIN is required to access this feature.",
					"Enter ADMINISTRATOR PIN:",
					"\n");
			print_options("* BACK", "", "OK #");
			menu_state = MENU_EEMEM_WFD;;
		}
		else if(menu_state == MENU_EEMEM_WFD){
			if(eep_read_in_progress == false){
				saved_admin_pin =  eep_mem_read_data;
				menu_state = MENU_WFI;
				wait_for_key();
				in_line_num = 3;
			}
		}
		else if(menu_state == MENU_WFI){
			get_number();
			if(num_entered_flag){
				// if the PIN is valid display administration menu
				if(num_in == BACKUP_ADMIN_PIN || num_in == saved_admin_pin){
					device_state = ADMIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				else if(num_in == KEYPAD_BACK){
					device_state = MAIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				// else display error message and go back to display options
				else{
					display_splash_msg("WARNING", "INVALID PIN!", 20);
					future_device_state = ADMIN_MENU_AUTH;
					future_menu_state = MENU_UPDATE_DISP;
				}
				num_entered_flag = false;
			}
		}
	}
	/*
	 * Administration menu
	 */
	else if(device_state == ADMIN_MENU){
		if(menu_state == MENU_UPDATE_DISP){
			print_title("ADMINISTRATION");
			print_menu_items("Select an option:",
					"",
					"1. STIM SETTINGS",
					"2. CODES",
					"3. STIM HISTORY",
					"4. CHANGE PIN",
					"\n");
			print_options("* BACK", "", "");
			menu_state = MENU_WFI;
			wait_for_key();
		}
		else if(menu_state == MENU_WFI){
			if(keypress_flag){
				if(keypress == '1'){
					eeprom_read_dword(EEMEM_STIM_SETTINGS_SETUP);
					device_state = STIM_SETTINGS;
					menu_state = MENU_EEMEM_WFD;
				}
				else if(keypress == '2'){
					eeprom_read_dword(EEMEM_CODES_GENERATED);
					device_state = CODES;
					menu_state = MENU_EEMEM_WFD;
				}
				else if(keypress == '3'){
					device_state = STIM_HISTORY;
					menu_state = MENU_UPDATE_DISP;
				}
				else if(keypress == '4'){
					device_state = CHANGE_PIN;
					menu_state = MENU_UPDATE_DISP;
				}
				else if(keypress == '*'){
					device_state = MAIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				wait_for_key();
			}
		}
	}
	/*
	 * Stim settings
	 */
	else if(device_state == STIM_SETTINGS){
		if(menu_state == MENU_EEMEM_WFD){
			if(!eep_read_in_progress){
				if(eep_mem_read_data == STIM_SETTINGS_SET_FLAG){
					display_splash_msg("INFO", "READING SETTINGS..", 20);
					future_device_state = STIM_SETTINGS;
					future_menu_state = MENU_EEMEM_WFD1;
					eeprom_read_dword(EEMEM_INTENSITY_G1);			//Read group 1 intensity
					menu_state = MENU_NOP;
				}
				else{
					device_state = STIM_SETTINGS_UPDATE;
					menu_state = MENU_UPDATE_DISP;
				}
			}
		}
		else if(menu_state == MENU_NOP){

		}
		else if(menu_state == MENU_EEMEM_WFD1){
			if(!eep_read_in_progress){
				g1_intensity = eep_mem_read_data;
				eeprom_read_dword(EEMEM_POLARITY_G1);			//Read group 1 polarity
				menu_state = MENU_EEMEM_WFD2;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD2){
			if(!eep_read_in_progress){
				g1_polarity = eep_mem_read_data;
				eeprom_read_dword(EEMEM_INTENSITY_G2);			//Read group 2 intensity
				menu_state = MENU_EEMEM_WFD3;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD3){
			if(!eep_read_in_progress){
				g2_intensity = eep_mem_read_data;
				eeprom_read_dword(EEMEM_POLARITY_G2);			//Read group 2 polarity
				menu_state = MENU_EEMEM_WFD4;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD4){
			if(!eep_read_in_progress){
				g2_polarity = eep_mem_read_data;
				eeprom_read_dword(EEMEM_DURATION);				//Read duration
				menu_state = MENU_EEMEM_WFD5;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD5){
			if(!eep_read_in_progress){
				duration = eep_mem_read_data;
				eeprom_read_dword(EEMEM_SHAM);					//Read SHAM
				menu_state = MENU_EEMEM_WFD6;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD6){
			if(!eep_read_in_progress){
				sham = eep_mem_read_data;
				menu_state = MENU_UPDATE_DISP;
			}
		}
		else if(menu_state == MENU_UPDATE_DISP){
			//Read and display settings
			print_title("STIM SETTINGS");
			if(g1_polarity == POLARITY_CENTRAL_ANODE){
				sprintf(str_g1_intensity, "INTENSITY (GROUP 1): %0.1f mA (CENTRAL ANODE)", (float)g1_intensity/1000.0f);
			}
			else if(g1_polarity == POLARITY_CENTRAL_CATHODE){
				sprintf(str_g1_intensity, "INTENSITY (GROUP 1): %0.1f mA (CENTRAL CATHODE)", (float)g1_intensity/1000.0f);
			}
			if(g2_polarity == POLARITY_CENTRAL_ANODE){
				sprintf(str_g2_intensity, "INTENSITY (GROUP 2): %0.1f mA (CENTRAL ANODE)", (float)g2_intensity/1000.0f);
			}
			else if(g2_polarity == POLARITY_CENTRAL_CATHODE){
				sprintf(str_g2_intensity, "INTENSITY (GROUP 2): %0.1f mA (CENTRAL CATHODE)", (float)g2_intensity/1000.0f);
			}
			sprintf(str_duration, "DURATION: %lu minutes", duration);

			if(sham == STIM_SHAM_ON){
				sprintf(str_sham, "SHAM: ON");
			}
			else if(sham == STIM_SHAM_OFF){
				sprintf(str_sham, "SHAM: OFF");
			}
			print_menu_items("SAVED STIM SETTINGS",
					"",
					str_g1_intensity,
					str_g2_intensity,
					str_duration,
					str_sham,
					"\n");
			print_options("* BACK", "", "UPDATE SETTINGS #");
			wait_for_key();
			menu_state = MENU_WFI;
		}
		else if(menu_state == MENU_WFI){
			if(keypress_flag){
				if(keypress == '*'){
					device_state = ADMIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				else if(keypress == '#'){
					device_state = STIM_SETTINGS_UPDATE;
					menu_state = MENU_UPDATE_DISP;
				}
				wait_for_key();
			}
		}
	}
	else if(device_state == STIM_SETTINGS_UPDATE){
		/*
		 * Get INTENSITY for GROUP 1
		 */
		if(menu_state == MENU_UPDATE_DISP){
			print_title("STIM SETTINGS");
			print_menu_items("Enter total INTENSITY for GROUP 1",
					"500uA - 4000uA (increment of 100uA)",
					"\n");
			print_options("* BACK", "", "OK #");
			menu_state = MENU_WFI;
			wait_for_key();
			num_entered_flag = false;
			in_line_num = 3;
		}
		else if(menu_state == MENU_WFI){
			get_number();
			if(num_entered_flag){
				// Check if the value is valid
				if(num_in == KEYPAD_BACK){
					device_state = ADMIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				else if(num_in < 500 || num_in > 4000){
					display_splash_msg("WARNING", "INVALID INPUT!", 20);
					future_device_state = STIM_SETTINGS_UPDATE;
					future_menu_state = MENU_UPDATE_DISP;
				}
				else{
					g1_intensity = num_in;
					g1_intensity /= 100;
					g1_intensity *= 100;
					menu_state = MENU_UPDATE_DISP1;
				}
				num_entered_flag = false;
			}
		}
		/*
		 * Get POLARITY for GROUP 1
		 */
		else if(menu_state == MENU_UPDATE_DISP1){
			print_title("STIM SETTINGS");
			print_menu_items("Select polarity (GROUP 1)",
					"1. CENTRAL ANODE",
					"2. CENTRAL CATHODE",
					"\n");
			print_options("* BACK", "", "");
			menu_state = MENU_WFI1;
			wait_for_key();
		}
		else if(menu_state == MENU_WFI1){
			if(keypress_flag){
				// Check if the value is valid
				if(keypress == '*'){
					device_state = ADMIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				else if(keypress == '1'){
					g1_polarity = POLARITY_CENTRAL_ANODE;
					menu_state = MENU_UPDATE_DISP2;
				}
				else if(keypress == '2'){
					g1_polarity = POLARITY_CENTRAL_CATHODE;
					menu_state = MENU_UPDATE_DISP2;
				}
			}
			wait_for_key();
		}
		/*
		 * Get INTENSITY for GROUP 2
		 */
		else if(menu_state == MENU_UPDATE_DISP2){
			print_title("STIM SETTINGS");
			print_menu_items("Enter total INTENSITY for GROUP 2",
					"500uA - 4000uA (increment of 100uA)",
					"\n");
			print_options("* BACK", "", "OK #");
			menu_state = MENU_WFI2;
			wait_for_key();
			num_entered_flag = false;
			in_line_num = 3;
		}
		else if(menu_state == MENU_WFI2){
			get_number();
			if(num_entered_flag){
				// Check if the value is valid
				if(num_in == KEYPAD_BACK){
					device_state = ADMIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				else if(num_in < 500 || num_in > 4000){
					display_splash_msg("WARNING", "INVALID INPUT!", 20);
					future_device_state = STIM_SETTINGS_UPDATE;
					future_menu_state = MENU_UPDATE_DISP2;
				}
				else{
					g2_intensity = num_in;
					g2_intensity /= 100;
					g2_intensity *= 100;
					menu_state = MENU_UPDATE_DISP3;
				}
				num_entered_flag = false;
			}
		}
		/*
		 * Get POLARITY for GROUP 2
		 */
		else if(menu_state == MENU_UPDATE_DISP3){
			print_title("STIM SETTINGS");
			print_menu_items("Select polarity (GROUP 2)",
					"1. CENTRAL ANODE",
					"2. CENTRAL CATHODE",
					"\n");
			print_options("* BACK", "", "");
			menu_state = MENU_WFI3;
			wait_for_key();
		}
		else if(menu_state == MENU_WFI3){
			if(keypress_flag){
				// Check if the value is valid
				if(keypress == '*'){
					device_state = ADMIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				else if(keypress == '1'){
					g2_polarity = POLARITY_CENTRAL_ANODE;
					menu_state = MENU_UPDATE_DISP4;
				}
				else if(keypress == '2'){
					g2_polarity = POLARITY_CENTRAL_CATHODE;
					menu_state = MENU_UPDATE_DISP4;
				}
			}
			wait_for_key();
		}
		/*
		 * Get DURATION
		 */
		else if(menu_state == MENU_UPDATE_DISP4){
			print_title("STIM SETTINGS");
			print_menu_items("Enter DURATION (5 - 40 minutes):",
							"\n");
			print_options("* BACK", "", "OK #");
			menu_state = MENU_WFI4;
			wait_for_key();
			in_line_num = 2;
			num_entered_flag = false;
		}
		else if(menu_state == MENU_WFI4){
			get_number();
			if(num_entered_flag){
				// Check if the value is valid
				if(num_in == KEYPAD_BACK){
					device_state = ADMIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				else if(num_in < 5 || num_in > 40){
					display_splash_msg("WARNING", "INVALID INPUT!", 20);
					future_device_state = STIM_SETTINGS_UPDATE;
					future_menu_state = MENU_UPDATE_DISP4;
				}
				else{
					duration = num_in;
					menu_state = MENU_UPDATE_DISP5;
				}
				num_entered_flag = false;
			}
		}
		/*
		 * Get SHAM
		 */
		else if(menu_state == MENU_UPDATE_DISP5){
			print_title("STIM SETTINGS");
			print_menu_items("SHAM condition:",
							"1. OFF",
							"2. ON",
							"\n");
			print_options("* BACK", "", "");
			menu_state = MENU_WFI5;
			wait_for_key();
		}
		else if(menu_state == MENU_WFI5){
			if(keypress_flag){
				// Check if the value is valid
				if(keypress == '*'){
					device_state = ADMIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				else if(keypress == '1'){
					sham = STIM_SHAM_OFF;
					eeprom_write_dword(EEMEM_INTENSITY_G1, g1_intensity);	// Write group 1 intensity
					menu_state = MENU_EEMEM_WFD;
				}
				else if(keypress == '2'){
					sham = STIM_SHAM_ON;
					eeprom_write_dword(EEMEM_INTENSITY_G1, g1_intensity);	// Write group 1 intensity
					menu_state = MENU_EEMEM_WFD;
				}
			}
			wait_for_key();
		}
		/*
		 * Write to EEPROM
		 */
		else if(menu_state == MENU_EEMEM_WFD){
			if(!eep_write_in_progress){
				eeprom_write_dword(EEMEM_POLARITY_G1, g1_polarity);			// Write group 1 polarity
				menu_state = MENU_EEMEM_WFD1;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD1){
			if(!eep_write_in_progress){
				eeprom_write_dword(EEMEM_INTENSITY_G2, g2_intensity);		// Write group 2 intensity
				menu_state = MENU_EEMEM_WFD2;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD2){
			if(!eep_write_in_progress){
				eeprom_write_dword(EEMEM_POLARITY_G2, g2_polarity);			// Write group 2 polarity
				menu_state = MENU_EEMEM_WFD3;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD3){
			if(!eep_write_in_progress){
				eeprom_write_dword(EEMEM_DURATION, duration);
				menu_state = MENU_EEMEM_WFD4;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD4){
			if(!eep_write_in_progress){
				eeprom_write_dword(EEMEM_SHAM, sham);
				menu_state = MENU_EEMEM_WFD5;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD5){
			if(!eep_write_in_progress){
				eeprom_write_dword(EEMEM_STIM_SETTINGS_SETUP, STIM_SETTINGS_SET_FLAG);
				menu_state = MENU_EEMEM_WFD6;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD6){
			if(!eep_write_in_progress){
				display_splash_msg("INFO", "STIM SETTINGS UPDATED!", 20);
				future_device_state = ADMIN_MENU;
				future_menu_state = MENU_UPDATE_DISP;
				menu_state = MENU_NOP;
			}
		}
		else if(menu_state == MENU_NOP){

		}
	}
	/*
	 * Code generation
	 */
	else if(device_state == CODES){
		// Check if code is already generated
		if(menu_state == MENU_EEMEM_WFD){
			if(!eep_read_in_progress){
				if(eep_mem_read_data == CODE_GEN_FLAG){
					// Code has been generated
					display_splash_msg("INFO", "READING CODES...", 24);
					future_device_state = CODES;
					future_menu_state = MENU_EEMEM_WFD1;
					read_codes_from_mem_flag = true;
					codes_read_complete_flag = false;
					menu_state = MENU_NOP;
				}
				else{
					display_splash_msg("INFO", "GENERATING CODES...", 24);
					future_device_state = CODES;
					future_menu_state = MENU_GP0;
					code_gen_en_flag = true;
					code_gen_complete_flag = false;
					srand(tim7_counter*tim10_counter*tim11_counter);
					menu_state = MENU_NOP;
				}
			}
		}
		else if(menu_state == MENU_EEMEM_WFD1){
			if(codes_read_complete_flag){
				menu_state = MENU_UPDATE_DISP;
			}
		}
		// Display generated codes
		else if(menu_state == MENU_UPDATE_DISP){
			print_title("CODES");
			for(uint32_t l_idx = 0; l_idx < 9; l_idx++){
				sprintf(mf_line_buffer[l_idx], "%lu  %lu  %lu  %lu  %lu  %lu  %lu  %lu  %lu",
						code_bank[(9*l_idx)+0], code_bank[(9*l_idx)+1], code_bank[(9*l_idx)+2], code_bank[(9*l_idx)+3], code_bank[(9*l_idx)+4],
						code_bank[(9*l_idx)+5], code_bank[(9*l_idx)+6], code_bank[(9*l_idx)+7], code_bank[(9*l_idx)+8]);
				update_mf_line_flag[l_idx] = true;
			}
			print_options("* BACK", "", "GENERATE NEW CODES #");
			menu_state = MENU_WFI;
			wait_for_key();
		}
		else if(menu_state == MENU_WFI){
			if(keypress_flag){
				if(keypress == '*'){
					device_state = ADMIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				else if(keypress == '#'){
					// Set flag to generate code
					display_splash_msg("INFO", "GENERATING CODES...", 20);
					future_device_state = CODES;
					future_menu_state = MENU_GP0;
					code_gen_en_flag = true;
					code_gen_complete_flag = false;
					srand(tim7_counter*tim10_counter*tim11_counter);
					menu_state = MENU_NOP;
				}
				wait_for_key();
			}
		}
		else if(menu_state == MENU_GP0){
			if(code_gen_complete_flag){
				eeprom_write_dword(EEMEM_CODES_GENERATED, CODE_GEN_FLAG);
				menu_state = MENU_EEMEM_WFD2;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD2){
			if(!eep_write_in_progress){
				display_splash_msg("INFO", "CODES GENERATED!", 20);
				future_device_state = ADMIN_MENU;
				future_menu_state = MENU_UPDATE_DISP;
				eeprom_read_dword(EEMEM_CODES_GENERATED);
				menu_state = MENU_NOP;
			}
		}
		else if(menu_state == MENU_NOP){	}
	}
	/*
	 * Stim history
	 */
	else if(device_state == STIM_HISTORY){
		if(menu_state == MENU_UPDATE_DISP){
			read_codes_from_mem_flag = true;
			codes_read_complete_flag = false;
			print_title("STIMULATION HISTORY");
			print_menu_items("ENTER STIM CODE:","\n");
			print_options("* BACK", "", "");
			menu_state = MENU_WFI;
			wait_for_key();
			in_line_num = 2;
		}
		else if(menu_state == MENU_WFI){
			get_number();
			if(num_entered_flag){
				if(num_in == KEYPAD_BACK){
					device_state = ADMIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				else{
					device_state = STIM_HISTORY;
					menu_state = MENU_EEMEM_WFD1;
				}
				num_entered_flag = false;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD1){
			if(codes_read_complete_flag){
				menu_state = MENU_GP0;
			}
		}
		else if(menu_state == MENU_GP0){			// Verify code here
			for(temp_idx = 0; temp_idx < MAX_CODES; temp_idx++){
				if(code_bank[temp_idx] == num_in){	// Code found
					break;
				}
			}
			menu_state = MENU_GP1;
		}
		else if(menu_state == MENU_GP1){
			if(temp_idx >= MAX_CODES){
				display_splash_msg("WARNING", "INVALID CODE!", 20);
				future_device_state = STIM_HISTORY;
				future_menu_state = MENU_UPDATE_DISP;
				menu_state = MENU_NOP;
			}
			else{
				if(code_use_flag[temp_idx] != 0xAA){
					display_splash_msg("WARNING", "CODE NOT USED!", 20);
					future_device_state = STIM_HISTORY;
					future_menu_state = MENU_UPDATE_DISP;
					menu_state = MENU_NOP;
				}
				else{
					code_idx = temp_idx;
					sprintf(str_stim_code, "ENTER STIM CODE: %lu", code_bank[code_idx]);
					eeprom_read_dword(EEMEM_UNIQUE_CRITICAL_ARRAY+(code_idx*4));
					menu_state = MENU_EEMEM_WFD2;
				}
			}
		}
		else if(menu_state == MENU_EEMEM_WFD2){
			if(!eep_read_in_progress){
				temp_critical_events = eep_mem_read_data;
				sprintf(str_critical_events, "CRITICAL EVENTS: %lu", temp_critical_events);
				eeprom_read_dword(EEMEM_CRITICAL_DURATION_ARRAY+(code_idx*4));
				menu_state = MENU_EEMEM_WFD3;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD3){
			if(!eep_read_in_progress){
				temp_critical_time = eep_mem_read_data;
				sprintf(str_critical_time, "CRITICAL TIME: 00:%02lu:%02lu", (temp_critical_time/60), (temp_critical_time%60));
				eeprom_read_dword(EEMEM_STIM_DURATION_ARRAY+(code_idx*4));
				menu_state = MENU_EEMEM_WFD4;
			}
		}
		else if(menu_state == MENU_EEMEM_WFD4){
			if(!eep_read_in_progress){
				temp_stim_time = eep_mem_read_data;
				sprintf(str_total_stim_time, "STIMULATION TIME: 00:%02lu:%02lu", (temp_stim_time/60), (temp_stim_time%60));
				menu_state = MENU_UPDATE_DISP1;
			}
		}
		else if(menu_state == MENU_UPDATE_DISP1){
			print_title("STIMULATION HISTORY");
			print_menu_items(str_stim_code,
					"",
					str_critical_events,
					str_critical_time,
					str_total_stim_time,
					"\n");
			print_options("* BACK", "", "");
			menu_state = MENU_WFI1;
			wait_for_key();
		}
		else if(menu_state == MENU_WFI1){
			if(keypress_flag){
				if(keypress == '*'){
					menu_state = MENU_UPDATE_DISP;
				}
				wait_for_key();
			}
		}
		else if(menu_state == MENU_NOP){

		}

	}
	/*
	 * Change pin
	 */
	else if(device_state == CHANGE_PIN){
		static uint32_t pin1;
		if(menu_state == MENU_UPDATE_DISP){
			print_title("CHANGE PIN");
			print_menu_items("Enter new PIN (3-7) digits",
							"\n");
			print_options("* BACK", "", "# OK");
			menu_state = MENU_WFI;
			wait_for_key();
			in_line_num = 2;
		}
		else if(menu_state == MENU_WFI){
			get_number();
			if(num_entered_flag){
				if(num_in == KEYPAD_BACK){
					device_state = ADMIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				// Check if the entered number is valid
				else if(num_in < 100 || num_in > 9999999){
					display_splash_msg("WARNING", "INVALID PIN!", 20);
					future_device_state = CHANGE_PIN;
					future_menu_state = MENU_UPDATE_DISP;
				}
				else{
					pin1 = num_in;
					menu_state = MENU_UPDATE_DISP1;
				}
				num_entered_flag = false;
			}
		}
		else if(menu_state == MENU_UPDATE_DISP1){
			print_title("CHANGE PIN");
			print_menu_items("Enter new PIN again (3-7) digits",
							"",
							"\n");
			print_options("* BACK", "", "# OK");
			menu_state = MENU_WFI1;
			wait_for_key();
			in_line_num = 2;
		}
		else if(menu_state == MENU_WFI1){
			get_number();
			if(num_entered_flag){
				if(num_in == KEYPAD_BACK){
					device_state = ADMIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				// Check if the entered number is valid
				else if(num_in < 100 || num_in > 9999999){
					display_splash_msg("WARNING", "INVALID PIN!", 20);
					future_device_state = CHANGE_PIN;
					future_menu_state = MENU_UPDATE_DISP;
				}
				else if(num_in != pin1){
					display_splash_msg("WARNING", "INVALID PIN!", 20);
					future_device_state = CHANGE_PIN;
					future_menu_state = MENU_UPDATE_DISP;
				}
				else{
					display_splash_msg("INFO", "PIN UPDATED", 20);
					eeprom_write_dword(EEMEM_ADMIN_PIN, pin1);
					future_device_state = ADMIN_MENU;
					future_menu_state = MENU_UPDATE_DISP;
				}
				num_entered_flag = false;
			}
		}
	}
	/*
	 * SUMMARY CODE
	 */
	else if(device_state == SUMMARY_CODE){
		if(menu_state == MENU_EEMEM_WFD){
			if(!eep_read_in_progress){
				temp_summary_code = eep_mem_read_data;
				sprintf(str_summary_code, "%010lu", temp_summary_code);
				menu_state = MENU_UPDATE_DISP;
			}
		}
		else if(menu_state == MENU_UPDATE_DISP){
			print_title("SUMMARY CODE");
			print_options("* BACK", "", "");
			print_menu_items(str_summary_code,"\n");
			wait_for_key();
			menu_state = MENU_WFI;
		}
		else if(menu_state == MENU_WFI){
			if(keypress_flag){
				if(keypress == '*'){
					device_state = MAIN_MENU;
					menu_state = MENU_UPDATE_DISP;
				}
				wait_for_key();
			}
		}
	}
	/*
	 * Shutdown device
	 */
	else if(device_state == SHUTDOWN){
		shutdown_device();
	}
	else if(device_state == BOOT_SCR){
		device_state = MAIN_MENU;
	}
	else if(device_state == WELCOME_SCR){
		device_state = MAIN_MENU;
	}
}


void shutdown_device(void){
	if(shutdown_counter == 0){
		beep_length = LONG_BEEP;
		shutdown_counter++;
	}
	else if(shutdown_counter == 1){
		if(beep_length == NO_BEEP){
			shutdown_counter++;
		}
	}
	else if(shutdown_counter == 2){
		// Wait for EEPROM task
		if(eep_read_in_progress || eep_write_in_progress) {
			return;
		}
		else{
			shutdown_counter++;
		}
	}
	else if(shutdown_counter > 2){
		HAL_GPIO_WritePin(PS_HOLD_GPIO_Port, PS_HOLD_Pin, GPIO_PIN_RESET);
	}
}

void app_code(void){
	static uint32_t gen_code_count = 0, temp_code, idx;
	static bool valid_code = true, write_to_mem_flag = false;
	// Generate code
	if(code_gen_en_flag){
		valid_code = true;
		if(gen_code_count < MAX_CODES){
			temp_code = rand()%100000;	// Gives random number <= 5 digits
			if(temp_code > 9999){		// Make sure the number is > 4 digits
				temp_code_bank[gen_code_count] = temp_code;	// Copy to temp code bank
				// Make sure the code doesn't repeat
				for(idx = 0; idx < gen_code_count; idx++){
					if(temp_code_bank[idx] == temp_code){
						valid_code = false;
					}
				}
				if(valid_code){
					gen_code_count++;
				}
			}
		}
		else{
			code_gen_en_flag = false;
			write_to_mem_flag = true;
			gen_code_count = 0;
		}
	}
	// Write codes to EEPROM
	else if(write_to_mem_flag == true){
		static uint32_t write_count = 0;
		if(write_count < MAX_CODES){
			if(eep_write_in_progress == false){
				eeprom_write_dword(EEMEM_CODES_ARRAY+(write_count*4), temp_code_bank[write_count]);
				write_count++;
			}
		}
		else{
			write_to_mem_flag = false;
			code_gen_complete_flag = true;
			write_count = 0;
		}
	}
	// Read codes from EEPROM
	else if(read_codes_from_mem_flag == true){
		static uint32_t read_count = 0;
		static bool data_copy_flag = false;
		if(read_count < MAX_CODES){
			// If read is not in progress and no data has to be copied then read the next code
			if(!eep_read_in_progress && data_copy_flag == false){
				eeprom_read_dword(EEMEM_CODES_ARRAY+(read_count*4));
				data_copy_flag = true;
			}
			// If read is not in progress and data is ready to be copied
			else if(!eep_read_in_progress && data_copy_flag == true){
				code_bank[read_count] = (0x00FFFFFF & eep_mem_read_data);
				code_use_flag[read_count] = eep_mem_read_data>>24;
				read_count++;
				data_copy_flag = false;
			}
		}
		else{
			codes_read_complete_flag = true;
			read_codes_from_mem_flag = false;
			read_count = 0;
		}
	}
	else{
		gen_code_count = 0;
		write_to_mem_flag = false;
		read_codes_from_mem_flag = false;
	}
}

void app_batt_voltage(void){
	if(adc_count >= 50){
		adc_accum = adc_accum/adc_count;
		adc_batt_avg = adc_accum;
		adc_accum = 0;
		batt_voltage = (float)adc_batt_avg*ADC_CONV_MUL;
		batt_percentage = (uint32_t)(((batt_voltage-LOW_BATT_VOLTAGE)/(MAX_BATT_VOLTAGE-LOW_BATT_VOLTAGE))*100.00f);
		if(batt_percentage < 0){
			batt_percentage = 0;
		}
		else if(batt_percentage > 100){
			batt_percentage = 100;
		}
		adc_count = 0;
		update_battery_flag = true;
	}
	else{
		adc_accum += adc_buffer[0];
		adc_count++;
	}
}
