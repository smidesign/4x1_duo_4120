/*
 * commands.c
 *
 *  Created on: Mar 23, 2021
 *      Author: SMI_DESIGN-05
 */

#include <stdbool.h>
#include "main.h"
#include "commands.h"

extern uint8_t g1_rmm_buffer[4];
uint8_t temp_g1_rmm_buffer[4];
extern uint8_t g2_rmm_buffer[4];
uint8_t temp_g2_rmm_buffer[4];
extern bool g1_update_rmm_flag, g2_update_rmm_flag;

uint8_t uart4_tx_buffer[UART_COM_SIZE], uart4_rx_buffer[UART_COM_SIZE];
uint8_t uart5_tx_buffer[UART_COM_SIZE], uart5_rx_buffer[UART_COM_SIZE];

uint8_t uart4_command, uart5_command;
uint32_t uart4_data, uart5_data;

extern UART_HandleTypeDef huart4;
extern UART_HandleTypeDef huart5;

uint32_t uart_byte_offset = 0;
void process_command_uart4(void){
	uart4_command = uart4_rx_buffer[0];
	//uart4_data = (uart4_rx_buffer[1]<<24) + (uart4_rx_buffer[2]<<16) + (uart4_rx_buffer[3]<<8)  + (uart4_rx_buffer[4]);
	if(uart4_command == RMM_DATA){
		for(uint8_t temp_idx = 0; temp_idx < 4; temp_idx++){
			g1_rmm_buffer[temp_idx] = uart4_rx_buffer[temp_idx+1];
			if(g1_rmm_buffer[temp_idx] != temp_g1_rmm_buffer[temp_idx]){
				temp_g1_rmm_buffer[temp_idx] = g1_rmm_buffer[temp_idx];
				g1_update_rmm_flag = true;
			}
		}
	}
	else{
		send_uart4(UART_SYNC_ERROR, NO_DATA);
		HAL_UART_AbortReceive(&huart4);
	}
}

void process_command_uart5(void){
	uart5_command = uart5_rx_buffer[0];
	//uart5_data = (uart5_rx_buffer[1]<<24) + (uart5_rx_buffer[2]<<16) + (uart5_rx_buffer[3]<<8)  + (uart5_rx_buffer[4]);
	if(uart5_command == RMM_DATA){
		for(uint8_t temp_idx = 0; temp_idx < 4; temp_idx++){
			g2_rmm_buffer[temp_idx] = uart5_rx_buffer[temp_idx+1];
			if(g2_rmm_buffer[temp_idx] != temp_g2_rmm_buffer[temp_idx]){
				temp_g2_rmm_buffer[temp_idx] = g2_rmm_buffer[temp_idx];
				g2_update_rmm_flag = true;
			}
		}
	}
	else{
		send_uart5(UART_SYNC_ERROR, NO_DATA);
		HAL_UART_AbortReceive(&huart5);
	}
}

void send_uart4(uint8_t cmd, uint32_t s_data){
	uart4_tx_buffer[0] = cmd;
	uart4_tx_buffer[1] = (uint8_t)(s_data>>24);
	uart4_tx_buffer[2] = (uint8_t)(s_data>>16);
	uart4_tx_buffer[3] = (uint8_t)(s_data>>8);
	uart4_tx_buffer[4] = (uint8_t)(s_data);
	HAL_UART_Transmit_DMA(&huart4, uart4_tx_buffer, UART_COM_SIZE);
}

void send_uart5(uint8_t cmd, uint32_t s_data){
	uart5_tx_buffer[0] = cmd;
	uart5_tx_buffer[1] = (uint8_t)(s_data>>24);
	uart5_tx_buffer[2] = (uint8_t)(s_data>>16);
	uart5_tx_buffer[3] = (uint8_t)(s_data>>8);
	uart5_tx_buffer[4] = (uint8_t)(s_data);
	HAL_UART_Transmit_DMA(&huart5, uart5_tx_buffer, UART_COM_SIZE);
}
