/*
 * AT25M02.c
 *
 *  Created on: Feb 11, 2021
 *      Author: SMI_DESIGN-05
 */

#include "AT25M02.h"

extern SPI_HandleTypeDef hspi1;
uint8_t eep_SRval, eep_write_data, eep_read_data, eep_poll_val;
uint32_t eep_mem_write_data, eep_mem_read_data;

uint32_t eep_address;

// Flags
bool eep_byte_write_en, eep_write_in_progress;
bool eep_byte_read_en, eep_read_in_progress;

uint8_t spi_eep_rw_array[8];
uint8_t temp_eep_mem_read_data[4];

// Write STATES
enum EEP_WR_ST{
	EEP_W_WREN,			// Set the AT25M02_WREN bit
	EEP_W_LPWP_WAIT1,		// Waiting for write to be over
	EEP_W_WR_DATA,		// Select , send write command(1), address(3) and data (1), deselect
	EEP_W_LPWP_WAIT2,		// Wait for write to complete
	EEP_W_BUSY_WAIT		// Wait while the device is busy with internal operation
}EEP_WRITE_STATE;
// Read STATES
enum EEP_RD_ST{
	EEP_R_WREN,
	EEP_R_LPWP_WAIT1,
	EEP_R_RD_DATA,		//Select, send read command(1) and address(3), read data(1), deselect
	EEP_R_LPWP_WAIT2,
	EEP_R_BUSY_WAIT
}EEP_READ_STATE;

void init_eeprom(void){
	// Pins initialized by HAL
	// SPI initialized by HAL
	// Disable write protect and hold
	EEPROM_WP_DIS;
	EEPROM_HOLD_DIS;
}

void eeprom_write_enable(void){
	eep_write_data = AT25M02_WREN;
	EEPROM_SELECT;
	HAL_SPI_Transmit(&hspi1, (uint8_t *)&eep_write_data, 1, 500);
	while(hspi1.State == HAL_SPI_STATE_BUSY);
	EEPROM_DESELECT;
}
void eeprom_write_disable(void){
	eep_write_data = AT25M02_WRDI;
	EEPROM_SELECT;
	HAL_SPI_Transmit(&hspi1, (uint8_t *)&eep_write_data, 1, 500);
	while(hspi1.State == HAL_SPI_STATE_BUSY);
	EEPROM_DESELECT;
}
uint8_t eeprom_read_sr(void){
	eep_write_data = AT25M02_RDSR;
	EEPROM_SELECT;
	HAL_SPI_Transmit(&hspi1, (uint8_t *)&eep_write_data, 1, 500);
	while(hspi1.State == HAL_SPI_STATE_BUSY);
	HAL_SPI_Receive(&hspi1,(uint8_t *)&eep_read_data, 1, 500);
	while(hspi1.State == HAL_SPI_STATE_BUSY);
	EEPROM_DESELECT;
	return eep_read_data;
}

void eeprom_write_sr(uint8_t data){
	uint8_t write_array[2] = { AT25M02_WRSR, data };
	EEPROM_SELECT;
	HAL_SPI_Transmit(&hspi1, (uint8_t *)write_array, 2, 500);
	while(hspi1.State == HAL_SPI_STATE_BUSY);
	EEPROM_DESELECT;
}

/**
 * \brief Checks if the device is busy
 * \return true if the device is busy, false if free
 */
bool eeprom_busy(void){
	eep_SRval = eeprom_read_sr();
	if((eep_SRval & AT25M02_SR_BSY_MSK) == AT25M02_SR_BSY){
		return true;
	}
	return false;
}

/**
 * \brief LOW POWER WRITE POLL
 * \return true if device is write busy false if free
 */
bool eeprom_LPWP_busy(void){
	eep_write_data = AT25M02_LPWP;
	eep_read_data = 0xFF;
	EEPROM_SELECT;
	HAL_SPI_Transmit(&hspi1, (uint8_t *)&eep_write_data, 1, 500);
	while(hspi1.State == HAL_SPI_STATE_BUSY);
	HAL_SPI_Receive(&hspi1, (uint8_t *)&eep_read_data, 1, 500);
	while(hspi1.State == HAL_SPI_STATE_BUSY);
	EEPROM_DESELECT;
	if(eep_read_data == 0x00){
		return false;
	}
	return true;
}

uint32_t eeprom_write_dword(uint32_t addr, uint32_t data){
	if(!eep_write_in_progress){
		eep_address = addr;
		eep_mem_write_data = data;
		eep_byte_write_en = true;
		return 0;
	}
	return 0xFF;
}

uint32_t eeprom_read_dword(uint32_t addr){
	if(!eep_read_in_progress){
		eep_address = addr;
		eep_byte_read_en = true;
		return 0;
	}
	return 0xFF;
}

void app_eeprom(void){
	if(eep_byte_write_en){
		eep_write_in_progress = true;
		if(EEP_WRITE_STATE == EEP_W_WREN){
			eeprom_write_enable();
			EEP_WRITE_STATE = EEP_W_LPWP_WAIT1;
			return;
		}
		else if(EEP_WRITE_STATE == EEP_W_LPWP_WAIT1){
			if(eeprom_LPWP_busy()){
				return;
			}
			EEP_WRITE_STATE = EEP_W_WR_DATA;
			return;
		}
		else if(EEP_WRITE_STATE == EEP_W_WR_DATA){
			spi_eep_rw_array[0] = (uint8_t)AT25M02_WRITE;
			spi_eep_rw_array[1] = (uint8_t)(eep_address>>16);
			spi_eep_rw_array[2] = (uint8_t)(eep_address >> 8);
			spi_eep_rw_array[3] = (uint8_t)(eep_address);
			spi_eep_rw_array[4] = (uint8_t)(eep_mem_write_data>>24);
			spi_eep_rw_array[5] = (uint8_t)(eep_mem_write_data>>16);
			spi_eep_rw_array[6] = (uint8_t)(eep_mem_write_data>>8);
			spi_eep_rw_array[7] = (uint8_t)(eep_mem_write_data);

			EEPROM_SELECT;
			HAL_SPI_Transmit(&hspi1, (uint8_t *)spi_eep_rw_array, 8, 500);
			while(hspi1.State == HAL_SPI_STATE_BUSY);
			EEPROM_DESELECT;
			EEP_WRITE_STATE = EEP_W_LPWP_WAIT2;
			return;
		}
		else if(EEP_WRITE_STATE == EEP_W_LPWP_WAIT2){
			if(eeprom_LPWP_busy()){
				return;
			}
			EEP_WRITE_STATE = EEP_W_BUSY_WAIT;
			return;
		}
		else if(EEP_WRITE_STATE == EEP_W_BUSY_WAIT){
			if(eeprom_busy()){
				return;
			}
			EEP_WRITE_STATE = EEP_W_WREN;
			eep_write_in_progress = false;
			eep_byte_write_en = false;
		}
	}
	else if(eep_byte_read_en){
		eep_read_in_progress = true;
		if(EEP_READ_STATE == EEP_R_WREN){
			eeprom_write_enable();
			EEP_READ_STATE = EEP_R_LPWP_WAIT1;
			return;
		}
		else if(EEP_READ_STATE == EEP_R_LPWP_WAIT1){
			if(eeprom_LPWP_busy()){
				return;
			}
			EEP_READ_STATE = EEP_R_RD_DATA;
			return;
		}
		else if(EEP_READ_STATE == EEP_R_RD_DATA){
			spi_eep_rw_array[0] = (uint8_t)AT25M02_READ;
			spi_eep_rw_array[1] = (uint8_t)(eep_address>>16);
			spi_eep_rw_array[2] = (uint8_t)(eep_address >> 8);
			spi_eep_rw_array[3] = (uint8_t)(eep_address);
			EEPROM_SELECT;
			HAL_SPI_Transmit(&hspi1, (uint8_t *)spi_eep_rw_array, 4, 500);
			while(hspi1.State == HAL_SPI_STATE_BUSY);
			HAL_SPI_Receive(&hspi1, (uint8_t *)&temp_eep_mem_read_data, 4, 500);
			while(hspi1.State == HAL_SPI_STATE_BUSY);
			EEPROM_DESELECT;
			eep_mem_read_data = (temp_eep_mem_read_data[0]<<24)+
					(temp_eep_mem_read_data[1]<<16)+
					(temp_eep_mem_read_data[2]<<8)+
					(temp_eep_mem_read_data[3]);
			EEP_READ_STATE = EEP_R_LPWP_WAIT2;
			return;
		}
		else if(EEP_READ_STATE == EEP_R_LPWP_WAIT2){
			if(eeprom_LPWP_busy()){
				return;
			}
			EEP_READ_STATE = EEP_R_BUSY_WAIT;
			return;
		}
		else if(EEP_READ_STATE == EEP_R_BUSY_WAIT){
			if(eeprom_busy()){
				return;
			}
			EEP_READ_STATE = EEP_R_WREN;
			eep_read_in_progress = false;
			eep_byte_read_en = false;
		}
	}
	else{
		eep_byte_write_en = false;
		eep_byte_read_en = false;
		eep_write_in_progress = false;
		eep_read_in_progress = false;
	}
}

