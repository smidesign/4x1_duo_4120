/*
 * commands.h
 *
 *  Created on: Mar 23, 2021
 *      Author: SMI_DESIGN-05
 */

#ifndef SRC_COMMANDS_H_
#define SRC_COMMANDS_H_

/* Commands */
#define COMMAND_SIZE			1 // bytes
#define DATA_SIZE				4 // bytes

#define NO_DATA					0x00
#define SET_STIM_INTENSITY		0x01
#define SET_POLARITY			0x02
#define SET_STIM_STATUS			0x03

#define RAMP_UP					0x10
#define RAMP_DOWN				0x11
#define ABORT_RAMP_DOWN			0x12

#define RMM_DATA				0x20

#define UART_SYNC_ERROR			0xAA

//
#define UART_COM_SIZE	5

#define CENTRAL_ANODE	1
#define CENTRAL_CATHODE 2

#define RMM_GOOD		1
#define RMM_MODERATE	2
#define RMM_POOR		3

void process_command_uart4(void);
void process_command_uart5(void);
void send_uart4(uint8_t cmd, uint32_t s_data);
void send_uart5(uint8_t cmd, uint32_t s_data);

#endif /* SRC_COMMANDS_H_ */
