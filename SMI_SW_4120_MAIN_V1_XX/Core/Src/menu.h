/*
 * menu.h
 *
 *  Created on: Mar 7, 2021
 *      Author: SMI_DESIGN-05
 */

#ifndef SRC_MENU_H_
#define SRC_MENU_H_

#include <stdbool.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include "AT25M02.h"
#include "keypad.h"
#include "buzzer.h"

// Defines the states of device.
// The state determines what menu is to be displayed on the screen.
enum devicestate{
	BOOT_SCR = 1,
	WELCOME_SCR,
	MAIN_MENU,
	STIM_MENU,
	ADMIN_MENU_AUTH,
	ADMIN_MENU,
	STIM_SETTINGS,
	STIM_SETTINGS_UPDATE,
	CODES,
	STIM_HISTORY,
	CHANGE_PIN,
	SUMMARY_CODE,
	SHUTDOWN
}device_state;

enum menustate{
	MENU_UPDATE_DISP = 100,	// Update display
	MENU_CHECK_LOW_BATT,
	MENU_EEMEM_WFD,		// EEPROM wait for data
	MENU_WFI,			// Wait For Input
	MENU_EEMEM_WRITE,
	MENU_UPDATE_DISP1,
	MENU_EEMEM_WFD1,
	MENU_WFI1,
	MENU_EEMEM_WRITE1,
	MENU_UPDATE_DISP2,
	MENU_EEMEM_WFD2,
	MENU_WFI2,
	MENU_EEMEM_WRITE2,
	MENU_UPDATE_DISP3,
	MENU_EEMEM_WFD3,
	MENU_WFI3,
	MENU_EEMEM_WRITE3,
	MENU_UPDATE_DISP4,
	MENU_EEMEM_WFD4,
	MENU_WFI4,
	MENU_EEMEM_WRITE4,
	MENU_UPDATE_DISP5,
	MENU_EEMEM_WFD5,
	MENU_WFI5,
	MENU_EEMEM_WRITE5,
	MENU_UPDATE_DISP6,
	MENU_EEMEM_WFD6,
	MENU_WFI6,
	MENU_EEMEM_WRITE6,
	MENU_UPDATE_DISP7,
	MENU_EEMEM_WFD7,
	MENU_WFI7,
	MENU_EEMEM_WRITE7,
	MENU_NOP,
	MENU_GP0,				// General purpose
	MENU_GP1,
	MENU_UART_SEND1,
	MENU_UART_SEND2,
	MENU_UART_SEND3,
}menu_state;

#define NO_STIM			0
#define SCAN_MODE 		1
#define STIM_MODE 		2
#define RAMPING_UP		3
#define RAMPING_DOWN	4
#define POST_STIM		5
#define PAUSE_MODE		6
#define SHAM_STIM		7



void print_menu_items(const char *prompt, ...);
void print_title(const char *title);
void print_options(const char *left_option, const char *center_option, const char *right_option);
void display_splash_msg(const char *title, const char *msg, uint32_t wait_time);
void app_menu(void);
void shutdown_device(void);
void app_splash_msg(void);
void app_code(void);
void app_batt_voltage(void);
#endif /* SRC_MENU_H_ */
