/*
 * AT25M02.h
 *
 *  Created on: Feb 11, 2021
 *      Author: SMI_DESIGN-05
 */

#ifndef SRC_AT25M02_H_
#define SRC_AT25M02_H_

#include <stdbool.h>
#include <stdio.h>
#include "main.h"
#include "eememAddr.h"


#define EEPROM_SIZE 262144		// Bytes
#define EEPROM_MAX_ADDRESS		(EEPROM_SIZE-4)

// AT25M02 INSTRUCTION SET
#define AT25M02_WRSR	0x01	//Write status register (SR)
#define AT25M02_WRITE	0x02	//Write to memory array
#define AT25M02_READ	0x03	//Read from memory array
#define AT25M02_WRDI	0x04	//Reset write enable latch (WEL)
#define AT25M02_RDSR	0x05	//Read status register (SR)
#define AT25M02_WREN	0x06	//Set write enable latch (WEL)
#define AT25M02_LPWP	0x08	//Low power write poll

// STATUS REGISTER (SR) BIT DEFINITIONS
#define AT25M02_SR_WPEN_0	0x00	//Write-Protect-Enable disable
#define AT25M02_SR_WPEN_1	0x80	//Write-protect-Enable enable
#define AT25M02_SR_BP_0		0x00	//Block write protect level 0 (No protection)
#define AT25M02_SR_BP_1		0x04	//Block write protect level 1 (25% protection)
#define AT25M02_SR_BP_3		0x08	//Block write protect level 2 (50% protection)
#define AT25M02_SR_BP_4		0x0C	//Block write protect level 3 (100% protection)
//#define AT25M02_SR_WEL_0	0x00	//Write-Enable-Latch disable
//#define AT25M02_SR_WEL_1	0x02	//Write-Enable-Latch enable

#define AT25M02_SR_RDY		0x00	//Device is ready for a new sequence
#define AT25M02_SR_BSY		0x01	//Device is busy with an internal operation
#define AT25M02_SR_BSY_MSK	0x01

#define AT25M02_LPWP_RDY	0x00	//Low Power Write Poll return: Ready
#define AT25M02_LPWP_BSY	0xFF	//Low Power Write Poll return: Busy


#define EEPROM_SELECT		HAL_GPIO_WritePin(SPI_NSS_MEM_GPIO_Port, SPI_NSS_MEM_Pin, GPIO_PIN_RESET)
#define EEPROM_DESELECT		HAL_GPIO_WritePin(SPI_NSS_MEM_GPIO_Port, SPI_NSS_MEM_Pin, GPIO_PIN_SET)

#define EEPROM_WP_EN		HAL_GPIO_WritePin(N_MEM_WP_GPIO_Port, N_MEM_WP_Pin, GPIO_PIN_RESET)
#define EEPROM_WP_DIS		HAL_GPIO_WritePin(N_MEM_WP_GPIO_Port, N_MEM_WP_Pin, GPIO_PIN_SET)
#define EEPROM_HOLD_EN		HAL_GPIO_WritePin(N_MEM_HOLD_GPIO_Port, N_MEM_HOLD_Pin, GPIO_PIN_RESET)
#define EEPROM_HOLD_DIS		HAL_GPIO_WritePin(N_MEM_HOLD_GPIO_Port, N_MEM_HOLD_Pin, GPIO_PIN_SET)


// Functions
void init_eeprom(void);
void eeprom_write_enable(void);
void eeprom_write_disable(void);
uint8_t eeprom_read_sr(void);
void eeprom_write_sr(uint8_t data);

bool eeprom_busy(void);
bool eeprom_LPWP_busy(void);

uint32_t eeprom_write_dword(uint32_t addr, uint32_t data);
uint32_t eeprom_read_dword(uint32_t addr);

void app_eeprom(void);
#endif /* SRC_AT25M02_H_ */
