/*
 * keypad.c
 *
 *  Created on: Feb 9, 2021
 *      Author: SMI_DESIGN-05
 */

#include "main.h"
#include "keypad.h"

// Holds the value of currently pressed key. 0xFF if no key is pressed
char temp_keypress = KEYPAD_NO_KEY;
// Holds the value of last valid key
char keypress = KEYPAD_NO_KEY;
char prev_keypress = KEYPAD_NO_KEY;
// Indicates that a key was pressed. Key value stored in keypress
// Clear this flag after reading the value
bool keypress_flag = false;
uint16_t keypress_timer = 0, idle_timer = 0;

char get_key(void){
	COL1_HIGH;	COL2_LOW;	COL3_LOW;
	// Careful here the rise/fall time of signal may affect the readings
	if(HAL_GPIO_ReadPin(ROW1_GPIO_Port, ROW1_Pin)){ return '1'; }
	else if(HAL_GPIO_ReadPin(ROW2_GPIO_Port, ROW2_Pin)){ return '4'; }
	else if(HAL_GPIO_ReadPin(ROW3_GPIO_Port, ROW3_Pin)){ return '7'; }
	else if(HAL_GPIO_ReadPin(ROW3_GPIO_Port, ROW4_Pin)){ return '*'; }

	COL1_LOW;	COL2_HIGH;	COL3_LOW;
	// Careful here the rise/fall time of signal may affect the readings
	if(HAL_GPIO_ReadPin(ROW1_GPIO_Port, ROW1_Pin)){ return '2'; }
	else if(HAL_GPIO_ReadPin(ROW2_GPIO_Port, ROW2_Pin)){ return '5'; }
	else if(HAL_GPIO_ReadPin(ROW3_GPIO_Port, ROW3_Pin)){ return '8'; }
	else if(HAL_GPIO_ReadPin(ROW3_GPIO_Port, ROW4_Pin)){ return '0'; }

	COL1_LOW;	COL2_LOW;	COL3_HIGH;
	// Careful here the rise/fall time of signal may affect the readings
	if(HAL_GPIO_ReadPin(ROW1_GPIO_Port, ROW1_Pin)){ return '3'; }
	else if(HAL_GPIO_ReadPin(ROW2_GPIO_Port, ROW2_Pin)){ return '6'; }
	else if(HAL_GPIO_ReadPin(ROW3_GPIO_Port, ROW3_Pin)){ return '9'; }
	else if(HAL_GPIO_ReadPin(ROW3_GPIO_Port, ROW4_Pin)){ return '#'; }

	COL1_LOW; 	COL2_LOW;	COL3_LOW;
	return KEYPAD_NO_KEY;
}

void wait_for_key(void){
	temp_keypress = KEYPAD_NO_KEY;
	keypress_flag = false;
}

void app_keypad(void){
	// Input scan (keypad and power button)
	temp_keypress = get_key();
	if(temp_keypress!= KEYPAD_NO_KEY && keypress_flag == false){
		keypress = temp_keypress;
		if(prev_keypress != keypress){
			keypress_timer = 0;	// Reset counter in not same
			keypress_flag = true;	// Set flag since its the first key press
		}
		prev_keypress = keypress;
	}
	else{
		prev_keypress = KEYPAD_NO_KEY;
	}
}


