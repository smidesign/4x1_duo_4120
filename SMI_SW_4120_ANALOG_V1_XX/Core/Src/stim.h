/*
 * stim.h
 *
 *  Created on: Mar 21, 2021
 *      Author: SMI_DESIGN-05
 */

#ifndef SRC_STIM_H_
#define SRC_STIM_H_

/*
 * 2051412000: B1=7.0, B2= 10.0f
 */
#define BOARD2

#ifdef BOARD1
#define STARTING_CURRENT 	8.0f
#define DAC_VREF			3.280f
#endif

#ifdef BOARD2
#define STARTING_CURRENT 	8.0f
#define DAC_VREF			3.321f

#endif

#define DACTOI_CONV_MUL		1.024f/DAC_VREF		// DAC to current conversion multiplier
												// dac_val = (Iset/(1000*2))x(4096/6.6)

#define ADC_BUFF_SIZE		8
#define RMM_BUFF_SIZE		4

#define CENTRAL_ANODE	1
#define CENTRAL_CATHODE 2

/* SCAN RMM IN stim.c
#define SCAN35K		2178
#define SCAN500K	2425

//#define SCAN35K	2160
//#define SCAN500K	2350
*/
#define STIM10V_CC	2770
#define STIM25V_CC	3576
#define STIM10V_CA	1498
#define STIM25V_CA	682
/* RMM B2 END */

#define RMM_GOOD		1
#define RMM_MODERATE	2
#define RMM_POOR		3

// Stimulation modes
#define SCAN_MODE		1
#define STIM_MODE		2

#define UART_COM_SIZE 	5

void set_output_current(int total_current);
void ramp_up(void);
void ramp_down(void);
void abort_ramp_down(void);
void app_ramp(void);

void adc_filter(void);

#endif /* SRC_STIM_H_ */
