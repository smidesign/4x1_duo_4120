/*
 * commands.c
 *
 *  Created on: Feb 5, 2021
 *      Author: SMI_DESIGN-05
 */


#include "main.h"
#include "commands.h"
#include "stim.h"
#include "DAC7565.h"
#include <stdbool.h>

extern uint8_t uart_rx_buffer[UART_COM_SIZE];
extern uint8_t uart_tx_buffer[UART_COM_SIZE];
extern uint8_t stim_status;
extern uint32_t total_intensity, polarity;

extern UART_HandleTypeDef huart4;
extern uint8_t uart_tx_buffer[UART_COM_SIZE];

uint8_t uart_command;
uint32_t uart_data;
bool uart_sync_error = false;
uint32_t byte_offset;
void process_command(void){
	uart_command = uart_rx_buffer[0];
	uart_data = (uint32_t)(uart_rx_buffer[1]<<24) +
		   (uint32_t)(uart_rx_buffer[2]<<16) +
		   (uint32_t)(uart_rx_buffer[3]<<8) +
		   (uint32_t)(uart_rx_buffer[4]);
	if(uart_command == SET_STIM_INTENSITY){
		total_intensity = uart_data;
	}
	else if(uart_command == SET_POLARITY){
		polarity = uart_data;
	}
	else if(uart_command == RAMP_UP){
		ramp_up();
	}
	else if(uart_command == RAMP_DOWN){
		ramp_down();
	}
	else if(uart_command == ABORT_RAMP_DOWN){
		abort_ramp_down();
	}
	else if(uart_command == SET_STIM_STATUS){
		stim_status = (uint8_t)uart_data;
	}
	else if(uart_command == SET_OUTPUT_TO_STARTING_CURRENT){
		DAC7565_update_all(STARTING_CURRENT);
	}
	else if(uart_command == UART_SYNC_ERROR){
		uart_sync_error = true;
	}
}
