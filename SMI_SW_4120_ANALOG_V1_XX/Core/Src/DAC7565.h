/*
 * DAC7565.h
 *
 *  Created on: Feb 5, 2021
 *      Author: SMI_DESIGN-05
 */

#ifndef INC_DAC7565_H_
#define INC_DAC7565_H_

#define DAC7565_DISABLE 	HAL_GPIO_WritePin(N_DAC_EN_GPIO_Port, N_DAC_EN_Pin, GPIO_PIN_SET)
#define DAC7565_ENABLE		HAL_GPIO_WritePin(N_DAC_EN_GPIO_Port, N_DAC_EN_Pin, GPIO_PIN_RESET)

#define DAC7565_LDAC_RESET	HAL_GPIO_WritePin(DAC_LDAC_GPIO_Port, DAC_LDAC_Pin, GPIO_PIN_RESET)
#define DAC7565_LDAC_SET	HAL_GPIO_WritePin(DAC_LDAC_GPIO_Port, DAC_LDAC_Pin, GPIO_PIN_SET)

// Functions
void init_DAC7565(void);
void DAC7565_reset(void);
void DAC7565_update_all(int value);

#endif /* INC_DAC7565_H_ */
