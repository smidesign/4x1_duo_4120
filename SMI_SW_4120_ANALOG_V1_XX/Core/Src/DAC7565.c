/*
 * DAC7565.c
 *
 *  Created on: Feb 5, 2021
 *      Author: SMI_DESIGN-05
 */


#include "main.h"
#include "DAC7565.h"

uint8_t data[3];
extern SPI_HandleTypeDef hspi1;

void init_DAC7565(void){
	// SPI is initialized
	// DAC7565 specific initialization here
	DAC7565_LDAC_RESET;
	// Disable the DAC (active low)
	DAC7565_DISABLE;
	// Reset the DAC
	DAC7565_reset();
	// Disable internal reference
	data[0] = 0x01; 	//Write to all channels with shift register data
	data[1] = 0x20;
	data[2] = 0x00;
	DAC7565_LDAC_RESET;
	DAC7565_ENABLE;
	HAL_GPIO_WritePin(N_DAC_SS_GPIO_Port, N_DAC_SS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, (uint8_t *)data, 3, 500);
	while(hspi1.State == HAL_SPI_STATE_BUSY);
	HAL_GPIO_WritePin(N_DAC_SS_GPIO_Port, N_DAC_SS_Pin, GPIO_PIN_SET);
	DAC7565_LDAC_SET;
	DAC7565_DISABLE;
	DAC7565_LDAC_RESET;
}

void DAC7565_reset(void){
	// Reset the DAC (1 ms pulse, active pulse)
	HAL_GPIO_WritePin(N_DAC_RST_GPIO_Port, N_DAC_RST_Pin, GPIO_PIN_RESET);
	HAL_Delay(1);
	HAL_GPIO_WritePin(N_DAC_RST_GPIO_Port, N_DAC_RST_Pin, GPIO_PIN_SET);
}


void DAC7565_update_all(int value){
	// Make sure that value is 12bit
	if(value > 4095){
		value = 4095;
	}
	// Last 4 bits are don't care
	value = (value<<4);
	data[0] = 0x34; 	//Write to all channels with shift register data
	data[1] = (uint8_t)(value>>8);
	data[2] = (uint8_t)(value);
	DAC7565_LDAC_RESET;
	DAC7565_ENABLE;
	HAL_GPIO_WritePin(N_DAC_SS_GPIO_Port, N_DAC_SS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, (uint8_t *)data, 3, 500);
	while(hspi1.State == HAL_SPI_STATE_BUSY);
	HAL_GPIO_WritePin(N_DAC_SS_GPIO_Port, N_DAC_SS_Pin, GPIO_PIN_SET);
	DAC7565_LDAC_SET;
	DAC7565_DISABLE;
	DAC7565_LDAC_RESET;
}
