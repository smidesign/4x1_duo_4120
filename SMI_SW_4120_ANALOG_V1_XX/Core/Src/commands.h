/*
 * commands.h
 *
 *  Created on: Feb 5, 2021
 *      Author: SMI_DESIGN-05
 */

#ifndef SRC_COMMANDS_H_
#define SRC_COMMANDS_H_

#define COMMAND_SIZE			1 // bytes
#define DATA_SIZE				4 // bytes

#define SET_STIM_INTENSITY		0x01
#define SET_POLARITY			0x02
#define SET_STIM_STATUS			0x03
#define SET_OUTPUT_TO_STARTING_CURRENT	0x04

#define RAMP_UP					0x10
#define RAMP_DOWN				0x11
#define ABORT_RAMP_DOWN			0x12

#define RMM_DATA				0x20

#define UART_SYNC_ERROR			0xAA

/*
#define NO_OP					0x0000
#define NO_DATA					0x00000000
#define ACK_OK					0xAAAAAAAA
#define ACK_ERR					0x55550000		// Replace 0000 with error codes

// COM
#define COMMUNICATION			0x01
#define COM_CHECK				0x0101

// DEVICE
#define DEVICE_SPECIFIC_PARAMS	0x02
#define GET_FIRMWARE_VERSION	0x0201
#define GET_BOARD_VERSION		0x0202

// STIM SETTINGS
#define SET_STIM_SETTINGS		0x03
#define SET_STIM_INTENSITY		0x0301
#define SET_STIM_DURATION		0x0302
#define SET_STIM_SHAM			0x0303
#define SET_STARTING_CURRENT	0x0304

// Get stim settings
#define GET_STIM_SETTINGS		0x04
#define GET_STIM_INTENSITY		0x0401
#define GET_STIM_DURATION		0x0402
#define GET_STIM_SHAM			0x0403
#define GET_STARTING_CURRENT	0x0404

// Stimulation and output control
#define STIMULATION				0x05
#define RAMP_UP					0x0501		//	Get to set current
#define RAMP_DOWN				0x0502		//	Get to starting current

#define SET_OUTPUT_CURRENT		0x0510
#define GET_OUTPUT_CURRENT		0x0511
*/
void process_command(void);

#endif /* SRC_COMMANDS_H_ */
