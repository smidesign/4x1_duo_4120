/*
 * stim.c
 *
 *  Created on: Mar 21, 2021
 *      Author: SMI_DESIGN-05
 */

#include "main.h"
#include "stim.h"
#include "DAC7565.h"
#include "commands.h"
#include <stdbool.h>


uint32_t adc_buffer[8];		// For all channels
uint32_t adc_accum[8];
uint32_t adc_buffer_filtered[8];
uint8_t rmm_buffer[4];
float current_per_channel;
float dac_val, final_dac_val, step;
uint8_t stim_status;
extern bool uart_sync_error;
// Stim parameters to be set by main
uint32_t total_intensity, polarity;
extern uint32_t byte_offset;

bool ramp_up_calc, ramp_up_flag;
bool ramp_down_calc, ramp_down_flag;
bool abort_ramp_down_calc, abort_ramp_down_flag;

bool uart_skip_transfer;

float current, ramp_time;
extern UART_HandleTypeDef huart4;
extern uint8_t uart_tx_buffer[UART_COM_SIZE];

#ifdef BOARD1
/* Resistance measurement B1 */
const uint32_t SCAN35K[4] = { 2165, 2160, 2165, 2185 };
const uint32_t SCAN500K[4] = { 2345, 2316, 2363, 2497 };
#endif
#ifdef BOARD2
/* Resistance measurement B2 */
const uint32_t SCAN35K[4] = { 2142, 2142, 2142, 2143 };
const uint32_t SCAN500K[4] = { 2475, 2395, 2380, 2502};
#endif
// Divide total current by 4
void set_output_current(int total_current){
	float current_per_channel = (float)total_current/4.0f;
	dac_val = (int)(current_per_channel * DACTOI_CONV_MUL);
	DAC7565_update_all(dac_val);
}

// Controls all ramps
uint32_t ramp_counter = 0;
void ramp_up(void){
	current = (float)total_intensity/4.0f;
	ramp_time = 30.0f;
	ramp_up_calc = true;
	ramp_up_flag = false;
	ramp_down_calc = false;
	ramp_down_flag = false;
	abort_ramp_down_calc = false;
	abort_ramp_down_flag = false;
}
void ramp_down(void){
	current = (float)total_intensity/4.0f;
	ramp_time = 30.0f;
	ramp_up_calc = false;
	ramp_up_flag = false;
	ramp_down_calc = true;
	ramp_down_flag = false;
	abort_ramp_down_calc = false;
	abort_ramp_down_flag = false;
}
void abort_ramp_down(void){
	// Use the existing DAC value and step
	ramp_up_calc = false;
	ramp_up_flag = false;
	ramp_down_calc = false;
	ramp_down_flag = false;
	abort_ramp_down_calc = true;
	abort_ramp_down_flag = false;
}
void app_ramp(void){
	if(ramp_up_calc){
		// Calculate ramp values
		dac_val = 0;
		if(polarity == CENTRAL_CATHODE){
			final_dac_val = current*DACTOI_CONV_MUL;
		}
		else if(polarity == CENTRAL_ANODE){
			final_dac_val = -(current*DACTOI_CONV_MUL);
		}
		ramp_time *= 5.0f;
		step = final_dac_val/ramp_time;
		ramp_counter = 0;
		// Set ramp flags
		ramp_up_calc = false;
		ramp_up_flag = true;
	}
	else if(ramp_up_flag){
		if(ramp_counter < ramp_time){
			DAC7565_update_all((int)dac_val);
			dac_val += step;
			/*
			if(polarity == CENTRAL_CATHODE){
				dac_val += step;
			}
			else if(polarity == CENTRAL_ANODE){
				dac_val -= step;
			}*/
			ramp_counter++;
		}
		else{
			DAC7565_update_all((int)final_dac_val);
			ramp_up_flag = false;
		}
	}
	else if(ramp_down_calc){
		// Calculate ramp_values
		//dac_val = current*DACTOI_CONV_MUL;
		if(polarity == CENTRAL_CATHODE){
			dac_val = current*DACTOI_CONV_MUL;
		}
		else if(polarity == CENTRAL_ANODE){
			dac_val = -(current*DACTOI_CONV_MUL);
		}
		final_dac_val = STARTING_CURRENT;
		ramp_time *= 5.0f;
		step = dac_val/ramp_time;
		ramp_counter = 0;
		// Set ramp flags
		ramp_down_calc = false;
		ramp_down_flag = true;
	}
	else if(ramp_down_flag){
		if(ramp_counter < ramp_time){
			DAC7565_update_all((int)dac_val);
			dac_val -= step;
			/*
			if(polarity == CENTRAL_CATHODE){
				dac_val -= step;
			}
			else if(polarity == CENTRAL_ANODE){
				dac_val += step;
			}*/
			ramp_counter++;
		}
		else{
			DAC7565_update_all((int)final_dac_val);
			ramp_down_flag = false;
		}
	}
	else if(abort_ramp_down_calc){
		//dac_val
		final_dac_val = STARTING_CURRENT;
		ramp_time = ramp_counter;
		//step
		ramp_counter = 0;
		// Set ramp_flags
		abort_ramp_down_calc = false;
		abort_ramp_down_flag = true;
	}
	else if(abort_ramp_down_flag){
		if(ramp_counter < ramp_time){
			DAC7565_update_all((int)dac_val);
			dac_val -= step;
			/*
			if(polarity == CENTRAL_CATHODE){
				dac_val -= step;
			}
			else if(polarity == CENTRAL_ANODE){
				dac_val += step;
			}
			*/
			ramp_counter++;
		}
		else{
			DAC7565_update_all((int)final_dac_val);
			abort_ramp_down_flag = false;
		}
	}
}

uint8_t adc_idx = 0, filter_count = 0, ch_idx = 0, buf_idx;
uint32_t temp_counter = 0;
void adc_filter(){
	if(filter_count < 100){
		for(adc_idx = 1; adc_idx < 8; adc_idx+=2){
			adc_accum[adc_idx] += adc_buffer[adc_idx];
		}
		filter_count++;
	}
	else{
		for(adc_idx = 0; adc_idx < 8; adc_idx++){
			adc_buffer_filtered[adc_idx] = adc_accum[adc_idx]/filter_count;
			adc_accum[adc_idx] = 0;
		}
		filter_count = 0;
		if(stim_status == SCAN_MODE){
			for(ch_idx = 0; ch_idx < 4; ch_idx++){
				buf_idx = (ch_idx*2)+1;
				if(adc_buffer_filtered[buf_idx] < SCAN35K[ch_idx]){
					rmm_buffer[ch_idx] = RMM_GOOD;
					uart_tx_buffer[ch_idx+1] = RMM_GOOD;
				}
				else if(adc_buffer_filtered[buf_idx] < SCAN500K[ch_idx]){
					rmm_buffer[ch_idx] = RMM_MODERATE;
					uart_tx_buffer[ch_idx+1] = RMM_MODERATE;
				}
				else{
					rmm_buffer[ch_idx] = RMM_POOR;
					uart_tx_buffer[ch_idx+1] = RMM_POOR;
				}
			}

		}
		else if(stim_status == STIM_MODE && polarity == CENTRAL_CATHODE){
			for(ch_idx = 0; ch_idx < 4; ch_idx++){
				buf_idx = (ch_idx*2)+1;
				if(adc_buffer_filtered[buf_idx] < STIM10V_CC){
					rmm_buffer[ch_idx] = RMM_GOOD;
					uart_tx_buffer[ch_idx+1] = RMM_GOOD;
				}
				else if(adc_buffer_filtered[buf_idx] < STIM25V_CC){
					rmm_buffer[ch_idx] = RMM_MODERATE;
					uart_tx_buffer[ch_idx+1] = RMM_MODERATE;
				}
				else{
					rmm_buffer[ch_idx] = RMM_POOR;
					uart_tx_buffer[ch_idx+1] = RMM_POOR;
				}
			}
			/* TEST CODE BEGIN */
			/* TEST CODE END */
		}
		else if(stim_status == STIM_MODE && polarity == CENTRAL_ANODE){
			for(ch_idx = 0; ch_idx < 4; ch_idx++){
				buf_idx = (ch_idx*2)+1;
				if(adc_buffer_filtered[buf_idx] > STIM10V_CA){
					rmm_buffer[ch_idx] = RMM_GOOD;
					uart_tx_buffer[ch_idx+1] = RMM_GOOD;
				}
				else if(adc_buffer_filtered[buf_idx] > STIM25V_CA){
					rmm_buffer[ch_idx] = RMM_MODERATE;
					uart_tx_buffer[ch_idx+1] = RMM_MODERATE;
				}
				else{
					rmm_buffer[ch_idx] = RMM_POOR;
					uart_tx_buffer[ch_idx+1] = RMM_POOR;
				}
			}
		}
		uart_tx_buffer[0] = RMM_DATA;
		if(uart_sync_error){
			//HAL_UART_Transmit_DMA(&huart4, uart_tx_buffer, UART_COM_SIZE-1);
			HAL_UART_AbortTransmit(&huart4);
			uart_sync_error = false;
		}
		else{
			HAL_UART_Transmit_DMA(&huart4, uart_tx_buffer, UART_COM_SIZE);
		}

	}
}
